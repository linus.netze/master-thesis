import random
import argparse
from pathlib import Path

from dotenv import load_dotenv

from thesis.ape import (
    prepare_data,
    generate_prompts,
    save_promtps,
    PROMPT_GEN_TEMPLATE,
    DEMOS_TEMPLATE,
)

SEED = 42


def _get_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("model_type", choices=["hf", "openai"])
    parser.add_argument("data_dir", type=Path)
    args = parser.parse_args()

    return args


def main():
    args = _get_arguments()
    data = prepare_data(args.data_dir, n_gen=25)
    data = data["generation"]

    if args.model_type == "openai":
        load_dotenv()
        random.seed(SEED)
    elif args.model_type == "hf":
        import transformers

        transformers.set_seed(SEED)

    config = {}

    prompts = generate_prompts(
        conf=config,
        base_conf=f"configs/bertscore_{args.model_type}.yaml",
        prompt_gen_template=PROMPT_GEN_TEMPLATE,
        demos_template=DEMOS_TEMPLATE,
        prompt_gen_data=data,
    )

    save_promtps(prompts, args.data_dir, args.model_type)


if __name__ == "__main__":
    main()
