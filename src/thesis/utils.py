from typing import TypeVar, Type, Any, TypeGuard

_T = TypeVar("_T")
_U = TypeVar("_U")


def is_list_of(val: list[Any], type: Type[_T]) -> TypeGuard[list[_T]]:
    """Check if a list contains only elements of a given type. Used for type checking."""
    return all(isinstance(element, type) for element in val)


def is_dict_of(
    val: dict[Any, Any], type_1: Type[_T], type_2: Type[_U]
) -> TypeGuard[dict[_T, _U]]:
    """Check if a list contains only elements of a given type. Used for type checking."""

    check_1 = all(isinstance(element, type_1) for element in val.keys())
    check_2 = (type_2 == Any) or all(
        isinstance(element, type_2) for element in val.values()
    )

    return check_1 and check_2
