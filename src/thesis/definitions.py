from typing import Literal, get_args

StrategiesType = Literal[
    "minimal",
    "few_shot_truthful",
    "few_shot_deceptive",
    "persona_young",
    "persona_old",
    "fake",
    "crowdworkers",
    "ape",
]
STRATEGIES = get_args(StrategiesType)

VeracityType = Literal["truthful", "deceptive"]
VERACITIES = get_args(VeracityType)

SourceType = Literal["hf", "openai"]
SOURCES = get_args(SourceType)

LENGTH_QUANTILES = [7, 21, 30, 42, 60, 294]
