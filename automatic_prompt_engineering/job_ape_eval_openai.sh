#!/usr/bin/zsh

#SBATCH -J ape_eval_openai
#SBATCH -o ./logs/ape_eval_openai.log
#SBATCH --gres=gpu:1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=48G
#SBATCH --time=05:00:00

echo $(date)
echo "Account: $SLURM_JOB_ACCOUNT"

if [[ -z "${SLURM_JOB_ID}" ]]; then
    export SLURM_ARRAY_TASK_ID=1
fi

echo "Activating conda environment"

# Activate conda environment
export CONDA_ROOT=$HOME/miniconda3
. $CONDA_ROOT/etc/profile.d/conda.sh
export PATH="$CONDA_ROOT/bin:$PATH"
conda activate thesis

echo "Starting python script"

python ./evaluate_prompts.py "openai" "/home/ln908953/Documents/master_thesis/repo/data"