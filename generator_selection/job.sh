#!/usr/bin/zsh

#SBATCH -J generator_selection
#SBATCH --gres=gpu:2
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=100G
#SBATCH --time=00:20:00
#SBATCH --array=1-6

echo $SETTING_NAME

if [[ -z "${SLURM_JOB_ID}" ]]; then
    export SLURM_ARRAY_TASK_ID=3
fi

# Activate conda environment
export CONDA_ROOT=$HOME/miniconda3
. $CONDA_ROOT/etc/profile.d/conda.sh
export PATH="$CONDA_ROOT/bin:$PATH"
conda activate thesis

# Define models to test
model_ids=("mistralai/Mistral-7B-Instruct-v0.1" "HuggingFaceH4/zephyr-7b-beta" "openchat/openchat_3.5" "lmsys/vicuna-13b-v1.5" "WizardLM/WizardLM-13B-V1.2" "meta-llama/Llama-2-13b-chat-hf")

echo ${model_ids[SLURM_ARRAY_TASK_ID]}
python test_generators.py ${model_ids[SLURM_ARRAY_TASK_ID]} "$SETTING_NAME" --results_dir "./results"