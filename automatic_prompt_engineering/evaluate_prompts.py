import random
import argparse
from pathlib import Path

from dotenv import load_dotenv

from thesis.ape import (
    load_promtps,
    prepare_data,
    evaluate_prompts,
    save_results,
    EVAL_TEMPLATE,
    DEMOS_TEMPLATE,
)

SEED = 42


def _get_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("model_type", choices=["hf", "openai"])
    parser.add_argument("data_dir", type=Path)
    args = parser.parse_args()

    return args


def main():
    args = _get_arguments()
    data = prepare_data(args.data_dir, n_gen=25)
    data = data["evaluation"]

    if args.model_type == "openai":
        load_dotenv()
        random.seed(SEED)
    elif args.model_type == "hf":
        import transformers

        transformers.set_seed(SEED)

    prompts = load_promtps(data_dir=args.data_dir, model_type=args.model_type)
    config = {"evaluation": {"checkpoint_dir": args.data_dir / "ape" / f"checkpoints_{args.model_type}"}}
    res = evaluate_prompts(
        prompts,
        eval_template=EVAL_TEMPLATE,
        demos_template=DEMOS_TEMPLATE,
        eval_data=data,
        conf=config,
        base_conf=f"configs/bertscore_{args.model_type}.yaml",
    )

    save_results(res, args.data_dir, model_type=args.model_type)


if __name__ == "__main__":
    main()
