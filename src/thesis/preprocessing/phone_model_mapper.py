from pathlib import Path
from re import Pattern
from collections.abc import Callable

import json

import pandas as pd


class PhoneModelMapper:
    def __init__(
        self,
        raw_data: pd.DataFrame,
        mapping_dir: Path,
        gsmarena_model_path: Path,
        load_existing_mappings: bool = True,
    ):
        self._raw_data = raw_data
        self._mapping_dir = mapping_dir

        with open(gsmarena_model_path, "r") as f:
            self._gsmarena_models = json.load(f)

        self._automatic_mappings: dict[str, dict[str, str]] = dict()
        self._full_mappings: dict[str, dict[str, str]] = dict()
        self._cleaned_data: pd.DataFrame | None = None

    @property
    def cleaned_data(self) -> pd.DataFrame:
        if self._cleaned_data is None:
            self._apply_mappings()
            assert self._cleaned_data is not None

        invalid_models = self.validate_models()

        if len(invalid_models) > 0:
            raise ValueError(
                f"Data still contains invalid models. This means that either mappings are missing or that mappings are wrong. The invalid models can be obtained by calling `PhoneModelMapper.validate_models()`."
            )

        return self._cleaned_data

    @property
    def automatic_mappings(self) -> dict[str, dict[str, str]]:
        return self._automatic_mappings

    def prepare_brand_mapping(
        self,
        brand: str,
        regex: Pattern | None = None,
        preprocess_function: Callable[[str], str] = lambda s: s.lower().strip(),
        overwrite: bool = False,
    ):
        models = self._get_model_list(brand)

        if regex is None:
            replace_map = {}
            not_found = models
        else:
            replace_map, not_found = self._find_automatic_mappings(
                models, regex, preprocess_function=preprocess_function
            )

        self._automatic_mappings[brand] = replace_map
        self._save_manual_mapping_template(brand, not_found, overwrite=overwrite)

    def _get_model_list(self, brand: str):
        models = self._raw_data.query(f"brand=='{brand}'").phone_type.unique().tolist()

        return models

    def _find_automatic_mappings(
        self,
        models: list[str],
        regex: Pattern,
        preprocess_function: Callable[[str], str],
    ) -> tuple[dict[str, str], list[str]]:
        replace_map = dict()
        not_found = list()
        for m in models:
            m_pre = preprocess_function(m)
            if r := regex.search(m_pre):
                m_found = r.group("model")
                replace_map[m] = m_found
            else:
                not_found.append(m)

        return replace_map, not_found

    def _save_manual_mapping_template(
        self, brand: str, models: list[str], overwrite: bool = False
    ):
        manual_replace_map = {m: "" for m in models}

        template_path = self._mapping_dir / f"model_replacement_{brand.lower()}.json"

        if template_path.exists() and not overwrite:
            return

        with open(template_path, "w") as f:
            json.dump(manual_replace_map, f, indent=2)

    def update_brand_with_manual_map(
        self,
        brand: str,
        postprocess_function: Callable[[str], str] = lambda s: s,
    ):
        automatic_brand_mapping = self._automatic_mappings[brand]
        manual_mapping = self._read_manual_mapping_template(brand)
        joined_mapping = automatic_brand_mapping | manual_mapping

        # Get an identity mapping for all models not contained in either automatic or manual map
        # i.e. those who have a correct name (exept for casing) not catched by automatic mapping
        unmapped_mapping = self._get_unmapped_model_map(joined_mapping, brand)
        full_mapping = joined_mapping | unmapped_mapping
        full_mapping = {k: postprocess_function(v) for k, v in full_mapping.items()}
        full_mapping = self._mapping_fix_casing(full_mapping, brand)

        self._full_mappings[brand] = full_mapping
        self._apply_mappings()

    def _read_manual_mapping_template(self, brand: str) -> dict[str, str]:
        template_path = self._mapping_dir / f"model_replacement_{brand.lower()}.json"

        with open(template_path, "r") as f:
            manual_replace_map = json.load(f)

        return manual_replace_map

    def _get_unmapped_model_map(
        self, incomplete_mapping: dict[str, str], brand: str
    ) -> dict[str, str]:
        idx_brand = self._raw_data.brand == brand
        df_brand = self._raw_data.loc[idx_brand]

        brand_models = set(df_brand.phone_type)
        mapped_models = set(incomplete_mapping.keys())
        unmapped_models = brand_models - mapped_models
        unmapped_map = {m: m for m in unmapped_models}

        return unmapped_map

    def _mapping_fix_casing(
        self, mapping: dict[str, str], brand: str
    ) -> dict[str, str]:
        gsmarena_models_brand = self._gsmarena_models[brand.lower()]
        gsmarena_models_brand_lower = [m.lower() for m in gsmarena_models_brand]

        fixed_mapping = mapping.copy()
        for k, v in mapping.items():
            try:
                idx = gsmarena_models_brand_lower.index(v.lower())
                fixed_mapping[k] = gsmarena_models_brand[idx]
            except:
                continue

        return fixed_mapping

    def get_gsmarena_mismatches(
        self,
        brand: str,
    ) -> dict[str, str]:
        mapping = self._full_mappings[brand]

        gsmarena_models_brand = [
            m.lower() for m in self._gsmarena_models[brand.lower()]
        ]

        mismatches = {
            k: v
            for k, v in mapping.items()
            if (v is None) or ((not v.lower() in gsmarena_models_brand) and v != "")
        }

        return mismatches

    def _apply_mappings(self):
        df = self._raw_data.copy()
        for brand, mapping in self._full_mappings.items():
            idx_brand = df.brand == brand
            df.loc[idx_brand, "phone_type"] = df.loc[idx_brand, "phone_type"].replace(
                mapping
            )

        self._cleaned_data = df

    def validate_models(self) -> dict[str, list[str]]:
        invalid_models = dict()

        if self._cleaned_data is None:
            raise ValueError("No mappings applied yet.")

        for brand in self._cleaned_data.brand.unique():
            gsm_brand_models = self._gsmarena_models[brand.lower()]

            idx_brand = self._cleaned_data.brand == brand
            df_brand = self._cleaned_data.loc[idx_brand]

            brand_models = df_brand.phone_type.unique()
            brand_invalid_models = [
                m for m in brand_models if (not m in gsm_brand_models and m != "")
            ]

            if len(brand_invalid_models) > 0:
                invalid_models[brand] = brand_invalid_models

        return invalid_models
