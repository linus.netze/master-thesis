import argparse
import json
import os
from pathlib import Path
from typing import NamedTuple

import pandas as pd

from datasets import Dataset

from thesis.classification import (
    get_condition,
    get_classifier_input,
    apply_data_split,
    ClassificationCondition,
    get_model_dir,
    get_evaluation_dir,
    get_evaluator,
    ModelType,
    load_model,
    CLASS_LABELS,
)
from thesis.classification.utils import DisableDatasetsProgressBar
from thesis import StrategiesType

EVALUATION_STRATEGIES: list[StrategiesType] = [
    "minimal",
    "few_shot_truthful",
    "few_shot_deceptive",
    "persona_young",
    "persona_old",
    "fake",
    "crowdworkers",
]


class Inputs(NamedTuple):
    approach: str
    cond: ClassificationCondition
    data_dir: Path
    model_dir: Path
    evaluation_dir: Path


def _get_inputs() -> Inputs:
    parser = argparse.ArgumentParser()
    parser.add_argument("task_id", type=int)
    parser.add_argument("approach")
    parser.add_argument("data_dir", type=Path)
    parser.add_argument("models_dir", type=Path)

    args = parser.parse_args()
    cond = get_condition(args.task_id, args.data_dir)

    inputs = Inputs(
        approach=args.approach,
        cond=cond,
        model_dir=get_model_dir(args.models_dir, args.approach, cond),
        data_dir=args.data_dir,
        evaluation_dir=get_evaluation_dir(args.data_dir, args.approach, cond),
    )

    if not inputs.evaluation_dir.exists():
        inputs.evaluation_dir.mkdir(parents=True)

    return inputs


def _load_data(data_dir: Path) -> Dataset:
    reviews = pd.read_csv(data_dir / "salminen" / "data_preprocessed.csv")

    reviews = (
        reviews.rename(columns={"review_text": "text"})
        .reset_index(names="review_id")
        .assign(title="")
        .assign(text=get_classifier_input)
        .loc[:, ["review_id", "text", "label"]]
    )

    # Transform to HuggingFace Dataset
    with DisableDatasetsProgressBar():
        data = Dataset.from_pandas(reviews, preserve_index=False).cast_column(
            "label", CLASS_LABELS
        )

    return data


def _evaluate_model(
    model: ModelType,
    reviews: Dataset,
    cond: ClassificationCondition,
    classification_threshold: float,
) -> pd.DataFrame:
    evaluator = get_evaluator(model)
    eval_results = evaluator(model, reviews, classification_threshold, batch_size=512)

    return eval_results


def main():
    inputs = _get_inputs()

    # Load data
    salminen_reviews = _load_data(inputs.data_dir)
    model, classification_threshold = load_model(inputs.model_dir, inputs.approach)

    # Evaluate
    results = _evaluate_model(
        model, salminen_reviews, inputs.cond, classification_threshold
    )
    results.to_csv(
        inputs.evaluation_dir / "evaluation_results_salminen.csv", index=False
    )


if __name__ == "__main__":
    main()
