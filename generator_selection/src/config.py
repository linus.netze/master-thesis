from pydantic import BaseModel
import argparse
import tomllib

from typing import Self, Any
from pathlib import Path


class GenerationArgs(BaseModel):
    do_sample: bool
    temperature: float
    max_new_tokens: int
    top_p: float
    top_k: float
    num_return_sequences: int


class Config(BaseModel):
    setting_name: str
    path: Path
    model_id: str
    generation: GenerationArgs

    @property
    def output_path(self) -> Path:
        model_id = self.model_id.replace("/", "_")
        return self.path / "generator_output" / f"{model_id}.json"

    @property
    def prompt_template_path(self) -> Path:
        return self.path / "instruction_templates.toml"

    @property
    def prompt_config_path(self) -> Path:
        return self.path / "prompt_configs.toml"

    @classmethod
    def from_cli_args_and_file(cls, cli_args: argparse.Namespace) -> Self:
        path = cli_args.results_dir / cli_args.setting_name

        config_path = path / "config.toml"
        file_config = _load_config_file(config_path)

        config = cls(
            setting_name=cli_args.setting_name,
            path=path,
            model_id=cli_args.model_id,
            **file_config,
        )

        return config


def _load_config_file(path: Path) -> dict[str, Any]:
    path = Path(path)
    with open(path, "rb") as f:
        config = tomllib.load(f)

    return config
