#!/usr/bin/zsh

#SBATCH -J salminen_inference
#SBATCH --gres=gpu:1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=48G
#SBATCH --time=00:10:00

echo "Account: $SLURM_JOB_ACCOUNT"

if [[ -z "${SLURM_JOB_ID}" ]]; then
    export SLURM_ARRAY_TASK_ID=1
fi

echo "Activating conda environment"

# Activate conda environment
export CONDA_ROOT=$HOME/miniconda3
. $CONDA_ROOT/etc/profile.d/conda.sh
export PATH="$CONDA_ROOT/bin:$PATH"
conda activate thesis

echo "Starting python script"

export MODELS_DIR="/hpcwork/ln908953/models"
export APPROACH="bert"
export DATA_DIR="../data/"

python ./salminen_inference.py "$SLURM_ARRAY_TASK_ID" "$APPROACH" "$DATA_DIR" "$MODELS_DIR"