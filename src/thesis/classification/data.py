import pandas as pd
import json

from pathlib import Path
from typing import NamedTuple

import datasets
from datasets import Dataset, DatasetDict, ClassLabel
from sklearn.model_selection import StratifiedKFold, train_test_split

from .classification import ClassificationCondition
from .utils import DisableDatasetsProgressBar

SEED = 42

CLASS_LABELS = ClassLabel(names=["human", "llm"])


class ReviewSets(NamedTuple):
    human: Dataset
    llm: Dataset


def create_data_splits(data: pd.DataFrame, k: int = 5) -> list[dict[str, list[int]]]:
    skf = StratifiedKFold(n_splits=k, shuffle=True, random_state=SEED)

    splits = []
    for train_valid_idx, test_idx in skf.split(data, data["veracity_label"]):
        train_valid_idx = data.iloc[train_valid_idx, :].index
        test_idx = data.iloc[test_idx].index

        train_idx, valid_idx = train_test_split(
            train_valid_idx,
            test_size=0.1,
            stratify=data.loc[train_valid_idx, "veracity_label"],
            shuffle=True,
            random_state=SEED,
        )
        split = {
            "train": train_idx.to_list(),
            "valid": valid_idx.to_list(),
            "test": test_idx.to_list(),
        }

        splits.append(split)

    return splits


def load_data(data_dir: Path) -> ReviewSets:
    human_reviews_pd = (
        pd.read_csv(data_dir / "human" / "review_data_preprocessed.csv", index_col="id")
        .rename(columns={"review_title": "title", "review_text": "text"})
        .rename_axis("review_id")
        .assign(label="human", text=get_classifier_input)
        .loc[
            (lambda df: (df.set_label == "classification")),
            ["text", "label", "veracity_label"],
        ]
    )

    llm_reviews_pd = (
        pd.read_json(
            data_dir / "generated" / "reviews.jsonl", orient="records", lines=True
        )
        .set_index(["review_id"])
        .assign(
            label="llm",
            veracity_label=lambda df: human_reviews_pd.loc[df.index, "veracity_label"],
            text=get_classifier_input,
        )
        .loc[
            (human_reviews_pd.index),
            ["text", "label", "strategy", "source", "veracity_label"],
        ]
    )

    # Transform to HuggingFace Dataset
    with DisableDatasetsProgressBar():
        human_reviews = Dataset.from_pandas(human_reviews_pd).cast_column(
            "label", CLASS_LABELS
        )

        llm_reviews = Dataset.from_pandas(llm_reviews_pd).cast_column(
            "label", CLASS_LABELS
        )

    review_sets = ReviewSets(human_reviews, llm_reviews)

    return review_sets


def get_classifier_input(data: pd.DataFrame) -> pd.Series:
    classifier_input = "Title: " + data["title"] + "\nText: " + data["text"]

    return classifier_input


def filter_training_data(
    reviews: ReviewSets, cond: ClassificationCondition
) -> ReviewSets:
    def human_filter(batch):
        return [v == cond.veracity for v in batch["veracity_label"]]

    def llm_filter(batch):
        has_source = [(s == cond.source) or (s is None) for s in batch["source"]]
        has_strategy = [(s == cond.strategy) or (s is None) for s in batch["strategy"]]
        has_veracity = [v == cond.veracity for v in batch["veracity_label"]]

        return [all(tup) for tup in zip(has_source, has_strategy, has_veracity)]

    with DisableDatasetsProgressBar():
        human_data = reviews.human.filter(human_filter, batched=True)
        llm_data = reviews.llm.filter(llm_filter, batched=True)

    filtered_reviews = ReviewSets(human_data, llm_data)

    return filtered_reviews


def filter_data_to_condition(
    reviews: Dataset, cond: ClassificationCondition, type: str
) -> Dataset:
    def filter_fn(batch: dict) -> list[bool]:
        has_veracity = [v == cond.veracity for v in batch["veracity_label"]]

        if reviews == "human":
            return has_veracity

        has_source = [(s == cond.source) or (s is None) for s in batch["source"]]
        has_strategy = [(s == cond.strategy) or (s is None) for s in batch["strategy"]]

        return [all(tup) for tup in zip(has_source, has_strategy, has_veracity)]

    with DisableDatasetsProgressBar():
        dataset = reviews.filter(filter_fn, batched=True)

    return dataset


def filter_by_strategies(reviews: Dataset, strategies: list) -> Dataset:
    def filter_fn(batch: dict) -> list[bool]:
        has_strategy = [(s in strategies) for s in batch["strategy"]]
        return has_strategy

    with DisableDatasetsProgressBar():
        dataset = reviews.filter(filter_fn, batched=True)

    return dataset


def apply_data_split(
    data: Dataset,
    split: dict[str, list[int]],
) -> DatasetDict:
    def filter_fn(batch: dict, set_name: str) -> list[bool]:
        in_set = [i in split[set_name] for i in batch["review_id"]]

        return in_set

    with DisableDatasetsProgressBar():
        dataset = DatasetDict(
            {
                s: data.filter(lambda b: filter_fn(b, s), batched=True)
                for s in split.keys()
            }
        )

    return dataset


def get_condition_dataset(
    reviews: ReviewSets, cond: ClassificationCondition
) -> DatasetDict:
    filtered_reviews = filter_training_data(reviews, cond)
    data = datasets.concatenate_datasets(list(filtered_reviews)).shuffle(seed=SEED)
    dataset = apply_data_split(data, cond.split)

    return dataset


def get_model_dir(
    models_dir: Path, approach: str, cond: ClassificationCondition
) -> Path:
    output_dir = (
        models_dir
        / approach
        / f"{cond.veracity}_{cond.source}_{cond.strategy}_{cond.split_id}"
    )

    return output_dir


def get_evaluation_dir(
    data_dir: Path, approach: str, cond: ClassificationCondition
) -> Path:
    output_dir = (
        data_dir
        / "classification"
        / "evaluation_results"
        / approach
        / f"{cond.veracity}_{cond.source}_{cond.strategy}_{cond.split_id}"
    )

    return output_dir
