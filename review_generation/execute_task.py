import argparse
from pathlib import Path

from thesis.generation import HuggingFaceTaskGenerator, OpenAiTaskGenerator
from dotenv import load_dotenv


def _get_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("task_id", type=int)
    parser.add_argument("model_type", choices=["huggingface", "openai"])
    parser.add_argument("--tasks_dir", type=Path)
    parser.add_argument("--data_dir", type=Path)
    parser.add_argument("--ignore_batches", action="store_true")

    args = parser.parse_args()

    if not args.tasks_dir.exists():
        raise Exception(f"Directory '{args.tasks_dir}' not found.")

    return args


def main():
    args = _get_arguments()

    if args.model_type == "openai":
        load_dotenv()
        generator = OpenAiTaskGenerator(
            task_id=args.task_id, tasks_dir=args.tasks_dir, data_dir=args.data_dir
        )
    elif args.model_type == "huggingface":
        generator = HuggingFaceTaskGenerator(
            task_id=args.task_id,
            tasks_dir=args.tasks_dir,
            data_dir=args.data_dir,
            ignore_batches=args.ignore_batches,
        )
    else:
        raise ValueError(f"Invalid model type: '{args.model_type}'")

    generator.generate_task()


if __name__ == "__main__":
    main()
