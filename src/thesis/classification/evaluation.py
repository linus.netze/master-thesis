from typing import Any, Callable
from itertools import chain

import numpy as np

from sklearn.metrics import roc_curve
from sklearn.linear_model import LogisticRegression
import evaluate

import pandas as pd

from setfit import SetFitModel
from transformers import PreTrainedModel, pipeline, AutoTokenizer
from transformers.pipelines.pt_utils import KeyDataset
from datasets import Dataset

import torch

from tqdm.auto import tqdm

from .data import CLASS_LABELS
from .classification import ModelType
from ..utils import is_list_of

ACCURACY = evaluate.load("accuracy")

_VectorType = np.ndarray | torch.Tensor


def compute_accuracy(
    y_true: _VectorType | list, y_score: _VectorType, threshold: float
) -> dict:
    predictions = np.where(y_score >= threshold, 1, 0)
    accuracy = ACCURACY.compute(predictions=predictions, references=y_true)
    assert isinstance(accuracy, dict)

    return accuracy


# Adapted from https://github.com/iamalegambetti/combat-ai-restaurants/blob/09254fdc86748884b1fc6c3f1a56a515b5f6e0cd/src/gpt/train.py#L56
def get_youdens_j(y_true, y_score):
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    idx = np.argmax(tpr - fpr)

    # youdens j is the same for each threshold in the interval [thresholds[idx], thresholds[idx + 1]]
    # use the mean of the interval as the threshold
    # more robust than using thresholds[idx] when the interval is large
    threshold_bounds = thresholds[idx : idx + 2]
    threshold = threshold_bounds.mean()

    return threshold


def get_evaluator(
    model: ModelType,
) -> Callable[[ModelType, Dataset, float, int | None], pd.DataFrame]:
    if isinstance(model, SetFitModel):
        return evaluate_setfit
    elif isinstance(model, PreTrainedModel):
        return evaluate_bert
    else:
        raise ValueError(f"Invalid model '{model}'.")


def evaluate_setfit(
    model: SetFitModel, data: Dataset, threshold: float
) -> pd.DataFrame:
    assert isinstance(model.model_head, LogisticRegression)
    pos_idx = list(model.model_head.classes_).index(1)

    texts = data["text"]
    scores = model.predict_proba(texts)[:, pos_idx]
    predictions = np.where(scores >= threshold, 1, 0)

    result = _format_evaluation_results(data, scores, predictions)

    return result


def evaluate_bert(
    model: PreTrainedModel,
    data: Dataset,
    youden_threshold: float,
    batch_size: int | None = None,
) -> pd.DataFrame:
    pipe = pipeline(
        "text-classification",
        model=model,
        tokenizer="roberta-large",
        device="cuda",
        truncation=True,
    )

    texts = KeyDataset(data, key="text")
    result = tqdm(pipe(texts, top_k=None, batch_size=batch_size), total=len(texts))

    pos_label = CLASS_LABELS.int2str(1)
    scores = np.array([x["score"] for x in chain(*result) if x["label"] == pos_label])
    predictions = np.where(scores >= 0.5, 1, 0)
    predictions_youden = np.where(scores >= youden_threshold, 1, 0)

    result = _format_evaluation_results(data, scores, predictions, predictions_youden)

    return result


def _format_evaluation_results(
    data: Dataset,
    scores: np.ndarray | torch.Tensor,
    predictions: np.ndarray,
    predictions_youden: np.ndarray,
) -> pd.DataFrame:
    df_data = data.to_pandas()
    assert isinstance(df_data, pd.DataFrame)

    columns = df_data.columns.intersection(["review_id", "label", "source", "strategy"])
    result = (
        df_data.loc[:, columns]
        .assign(
            scores=scores,
            predictions=predictions,
            predictions_youden=predictions_youden,
        )
        .assign(
            label=lambda df: df["label"].map(CLASS_LABELS.int2str),
            predictions=lambda df: df["predictions"].map(CLASS_LABELS.int2str),
            predictions_youden=lambda df: df["predictions_youden"].map(
                CLASS_LABELS.int2str
            ),
        )
    )

    return result
