# Adapted from https://jwalton.info/Matplotlib-latex-PGF/

import matplotlib.pyplot as plt
import seaborn as sns
import contextlib

TEXTWIDTH_PT = 396.80925


def get_figsize(
    aspectratio: str | float = "golden",
    fraction: float = 1,
    latex_subfigures: tuple[int, int] = (1, 1),
    subplots: tuple[int, int] = (1, 1),
):
    """Get figure dimensions to sit nicely in our document.

    Parameters
    ----------
    aspectratio:
        Ratio of height and width: (height/width)
    fraction:
        Fraction of the width which you wish the figure to occupy
    latex_subfigures:
        The number of rows and columns of subfigures in the latex doc.
    subplots:
        The number of rows and columns of subplots.

    Returns
    -------
    fig_dim:
        Dimensions of figure in inches
    """

    width_pt = TEXTWIDTH_PT

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    if aspectratio == "golden":
        aspectratio = (5**0.5 - 1) / 2

    assert isinstance(aspectratio, float)

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt / latex_subfigures[0]
    # Figure height in inches
    fig_height_in = (
        fig_width_in
        * aspectratio
        * (subplots[0] / subplots[1])
        * (latex_subfigures[0] / latex_subfigures[1])
    )

    return (fig_width_in, fig_height_in)


THESIS_RC = {
    "font.family": "serif",
    # "font.size": 11,
    "text.usetex": True,
    "pgf.rcfonts": False,
    # "xtick.major.pad": 1,
    # "ytick.major.pad": 1,
    # "axes.labelpad": 3,
    # "ytick.labelsize": 6,
    # "xtick.labelsize": 6,
    # "savefig.bbox": "tight",
    "lines.linewidth": 0.75,
    "legend.fancybox": False,
    "legend.framealpha": 1,
    "legend.edgecolor": "inherit",
    # "legend.fontsize": 6,
    "figure.dpi": 180,
    "savefig.dpi": 300,
    "figure.figsize": get_figsize(),
    "pgf.texsystem": "lualatex",
    "pgf.preamble": "\n".join(
        [
            r"\usepackage[utf8]{inputenc}",
            r"\usepackage[T1]{fontenc}",
            r"\usepackage{microtype}",
            r"\usepackage{lmodern}",
            r"\usepackage{palatino}",
        ]
    ),
    "scatter.marker": "o",
    "figure.constrained_layout.use": True,
    "figure.constrained_layout.h_pad": 0.01,
    "figure.constrained_layout.w_pad": 0.01,
    "figure.constrained_layout.wspace": 0.1,
    # "axes.titlesize": 9,
    "lines.markeredgecolor": "white",
}


def set_matplotlib_style():
    """Sets matplotlib rcParams to get consistent look of figures."""
    plt.rcParams.update(THESIS_RC)


@contextlib.contextmanager
def thesis_figure_style(rc_args=None, sns_style_args=None):

    rc_args = rc_args or dict()
    rc_args = THESIS_RC | rc_args
    sns_style_args = sns_style_args or dict()

    with (
        sns.plotting_context("paper") as sns_ctx,
        sns.axes_style(**sns_style_args) as sns_style,
        plt.rc_context(rc_args) as plt_ctx,
    ):
        yield sns_ctx, sns_style, plt_ctx
