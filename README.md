# Master thesis

This repo contains the code used for the master thesis "Spotting LLM-generated fake reviews: How prompt and model choice impact detectability" by Linus Netze. 

## Structure

This repository is structured into multiple folders for the different steps as well as the data and code package used in this thesis. Additional documentation is provided in the README of the respective directories.

- `preprocessing`: Contains notebooks and scripts used for preprocessing the dataset of human review datasets
- `generator_selection`: Contains the notebooks and scripts used for selecting the LLM which is used for review generation in the remainder of this thesis.
- `review_generation`: Contains the notebooks and scripts used for generating reviews, postprocessing them and doing a small data analysis
- `classification`: Contains the notebooks and scripts used for training and evaluating classification models.
- `data`: Contains the data used by and produced in this thesis, i.e. review datasets (human-written and LLM-generated), classification results, SHAP values
- `automatic_prompt_engineering`: Contains code for automatic creation of prompts. APE is not included in the thesis and only kept for archiving purposes. May not work.
- `src`: Contains a python package providing the code dependencies used by the various scripts and notebooks (see [here](#setting-up-python-the-environment))


## Getting data and models
Most of the data used and created in this thesis is provided in this repository. The classification models on the other hand are not provided due to storage limiations as they have a size ~250GB. They are thus only available upon request.

## Setting up the python environment

To execute the code in this repo you need to setup a conda envrionment using the provided environment file:

```
conda env create -f environment.yml
```

Further this repo contains a python package containing various code dependencies required for executing the scripts/notebooks. This package can be installed with:

```
pip install .
```

Because I use pgf as export format for my matplotlib plots, the execution of notebooks creating such plots requires you to have LaTex installed on your system.