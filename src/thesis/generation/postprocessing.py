import re
import json

from pathlib import Path
from itertools import product

import pandas as pd

from json_repair import repair_json


REVIEW_PATTERN = re.compile(
    r"\[\s*(?:\{\s*\"title\": \".+?\",\s*\"text\": \".+?\"\s*\},?\s*)+\]"
)

JSON_PATTERN = re.compile(r"^.*?(?P<json_str>[\{\[].*[\}\]]).*?$", re.DOTALL)

RESULTS_DIR = Path("./generator_selection/results")


def apply_manual_json_fixes(outputs: pd.Series, fixes_path: Path) -> pd.Series:
    fixes = pd.read_json(fixes_path, lines=True).set_index(
        ["review_id", "strategy", "source"]
    )

    outputs_fixed = outputs.copy()
    outputs_fixed[fixes.index] = fixes["output"]

    return outputs_fixed


def parse_review(raw_output: str) -> dict[str, str] | None:
    try:
        raw_json = _extract_json(raw_output)
        review = repair_json(raw_json, True)
        assert isinstance(review, dict)

        _validate_review(review)
    except:
        return None

    return review


def collect_generated_reviews(tasks_dir: Path, source: str) -> pd.DataFrame:
    reviews = []
    for task_dir in _get_task_dirs(tasks_dir):
        task_review_path = task_dir / "results.jsonl"
        if not task_review_path.exists():
            continue

        task_id = int(task_dir.name)
        task_reviews = pd.read_json(task_review_path, lines=True).assign(
            task_id=task_id
        )
        reviews.append(task_reviews)

    reviews = pd.concat(reviews).set_index(["review_id", "strategy"])

    # Remove duplicate reviews, only keep the most recent one
    reviews = (
        reviews.sort_values(["task_id"])
        .loc[lambda df: ~df.index.duplicated(keep="last")]
        .drop(columns=["task_id"])
    )

    # Add source to index
    reviews = reviews.assign(source=source).set_index("source", append=True)

    return reviews


def validate_review_completeness(
    reviews: pd.DataFrame, review_batch_map: pd.DataFrame
) -> None:
    # Bring review_batch_map into the same format as the index of reviews
    review_batch_map_idx = _review_batch_map_to_idx(review_batch_map)

    # Check if the indices are the same
    index_differences = reviews.set_index(
        "batch_id", append=True
    ).index.symmetric_difference(review_batch_map_idx)

    if not index_differences.empty:
        raise Exception("review_batch_map_idx and reviews do not have the same index")


def get_missing_batches(
    reviews: pd.DataFrame,
    review_batch_map: pd.DataFrame,
    strategies: list[str],
    sources: list[str] = ["hf", "openai"],
) -> dict[str, dict[str, list[int]] | str]:
    missing_batches = {s: dict() for s in sources}
    for strategy, source in product(strategies, sources):
        batch_id_column = f"{source}_batch_id"
        expected_batches = set(
            review_batch_map.loc[lambda df: (df.strategy == strategy)][batch_id_column]
        )
        try:
            review_subset = reviews.loc[pd.IndexSlice[:, strategy, source], :]
        except KeyError:
            missing_batches[source][strategy] = "all"
            continue

        present_batches = set(review_subset["batch_id"])
        if present_batches == expected_batches:
            continue

        missing_batches[source][strategy] = list(expected_batches - present_batches)

    return missing_batches


def get_error_messages(tasks_dir: Path) -> dict[str, list[str]]:
    error_messages = dict()
    for task_dir in _get_task_dirs(tasks_dir):
        task_errors_path = task_dir / "errors.json"

        if not task_errors_path.exists():
            continue

        with open(task_errors_path) as f:
            task_error_messages = json.load(f)

        strategy = _get_strategy_name(task_dir)
        error_messages |= {
            (strategy, batch_id): message
            for batch_id, message in task_error_messages.items()
        }

    return error_messages


def _extract_json(text: str) -> str:
    match = re.search(JSON_PATTERN, text)

    if match is None:
        raise ValueError("No JSON found")

    json_str = match["json_str"]

    return json_str


def _validate_review(review: dict[str, str]):
    if (not "title" in review) or (not "text" in review):
        raise Exception("Invalid review")


def _get_task_dirs(tasks_dir: Path) -> list[Path]:
    return [x for x in tasks_dir.iterdir() if x.is_dir()]


def _get_strategy_name(task_dir: Path) -> str:
    task_definition_path = task_dir / "definition.json"
    with open(task_definition_path) as f:
        task_definition = json.load(f)

    return task_definition["strategy"]


def _review_batch_map_to_idx(review_batch_map: pd.DataFrame) -> pd.Index:
    review_batch_map_idx = (
        review_batch_map.rename(
            columns={"hf_batch_id": "batch_id_hf", "openai_batch_id": "batch_id_openai"}
        )
        .pipe(
            pd.wide_to_long,
            stubnames="batch_id",
            i=["review_id", "strategy"],
            j="source",
            sep="_",
            suffix=r"(hf|openai)",
        )
        .set_index("batch_id", append=True)
        .index
    )

    return review_batch_map_idx
