import re
import json

from collections.abc import Generator
from pathlib import Path
from os import PathLike

from json_repair import repair_json

from src.utils import is_list_of

SETTING_NAME = "single_reviews_word_categories"

REVIEW_PATTERN = re.compile(
    r"\[\s*(?:\{\s*\"title\": \".+?\",\s*\"text\": \".+?\"\s*\},?\s*)+\]"
)

JSON_PATTERN = re.compile(r"^.*?(?P<json_str>[\{\[].*[\}\]]).*?$", re.DOTALL)

RESULTS_DIR = Path("./generator_selection/results")


def _extract_json(text: str) -> str:
    match = re.search(JSON_PATTERN, text)

    if match is None:
        raise ValueError("No JSON found")

    json_str = match["json_str"]

    return json_str


def _parse_reviews(texts: list[str] | str) -> list[dict]:
    # Depending on wether reviews are generated individually or combined, texts can be either
    # a list of json strings or a single json string
    if isinstance(texts, list):
        json_reviews = [_extract_json(text) for text in texts]
        reviews = [repair_json(review, True) for review in json_reviews]
    elif isinstance(texts, str):
        text = _extract_json(texts)
        reviews = repair_json(text, True)
        assert isinstance(reviews, list)
    else:
        raise ValueError("Invalid text")

    assert is_list_of(reviews, dict)

    return reviews


def _validate_reviews(reviews: list[dict]):
    for review in reviews:
        if (not "title" in review) or (not "text" in review):
            raise Exception("Invalid review")


def _save_results(path: Path, reviews: list[dict], failed: list[dict]):
    output_path = path / "extracted_reviews.json"
    with open(output_path, "w") as f:
        json.dump(reviews, f, indent=4)

    failed_path = path / "failed.json"
    with open(failed_path, "w") as f:
        json.dump(failed, f, indent=4)


def _get_raw_outputs(
    path: Path,
) -> Generator[tuple[str, str, str | list[str]], None, None]:
    generator_output_dir = path / "generator_output"

    for model_result_path in generator_output_dir.iterdir():
        model = model_result_path.stem

        with open(model_result_path) as f:
            results = json.load(f)

        for task, task_output in results.items():
            text = task_output["text"]
            yield model, task, text


def main(path: str | PathLike):
    path = Path(path)

    failed = list()
    reviews = list()
    for model, task, text in _get_raw_outputs(path):
        try:
            task_reviews = _parse_reviews(text)
            _validate_reviews(task_reviews)
        except ValueError:
            failed.append({"model": model, "task": task})
            continue

        metadata = {"model": model, "task": task}
        task_reviews = [metadata | review for review in task_reviews]
        reviews.extend(task_reviews)

    _save_results(path, reviews, failed)


if __name__ == "__main__":
    path = RESULTS_DIR / SETTING_NAME
    main(path)
