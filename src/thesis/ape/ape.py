from automatic_prompt_engineer import ape
import pandas as pd
import json
import re
from pathlib import Path

from thesis.generation.prompts import LENGTH_QUANTILES, RATING_TO_WORD

SEED = 42

EVAL_TEMPLATE = "[PROMPT]\n\nInput: [INPUT]"

PROMPT_GEN_TEMPLATE = (
    "I gave a friend an instruction. Based on the instruction they produced the following input-output pairs:\n"
    "\n"
    "[full_DEMO]\n"
    "\n"
    "Provide the instruction I gave to my friend as a quote."
)

DEMOS_TEMPLATE = "Input: [INPUT]\nOutput: [OUTPUT]\n###"


def prepare_data(data_dir: Path, n_gen: int) -> dict[str, tuple[list[str], list[str]]]:
    data_path = data_dir / "human" / "review_data_preprocessed.csv"
    data = (
        pd.read_csv(data_path)
        .loc[lambda df: (df.set_label == "prompt") & (df.veracity_label == "truthful")]
        .sample(frac=1, random_state=SEED)
    )

    input_data_records = (
        data.assign(length_category=lambda df: df.length_category.astype(int))
        .assign(
            min_length=lambda df: df.length_category.apply(
                lambda x: LENGTH_QUANTILES[x]
            ),
            max_length=lambda df: df.length_category.apply(
                lambda x: LENGTH_QUANTILES[x + 1]
            ),
            rating_in_words=lambda df: df.review_rating.map(RATING_TO_WORD),
            format="json",
        )
        .rename(columns={"review_rating": "rating"})
        .loc[
            :,
            [
                "brand",
                "phone_type",
                "rating",
                "rating_in_words",
                "min_length",
                "max_length",
                "format",
            ],
        ]
        .to_dict(orient="records")
    )
    input_data = [json.dumps(record) for record in input_data_records]
    # Remove extra whitespace (see https://github.com/Tiiiger/bert_score?tab=readme-ov-file#practical-tips)
    input_data = [re.sub(r"\s+", " ", i) for i in input_data]

    output_data_records = (
        data.loc[:, ["review_title", "review_text"]]
        .rename(columns={"review_title": "title", "review_text": "text"})
        .to_dict(orient="records")
    )

    output_data = [json.dumps(record) for record in output_data_records]
    # Remove extra whitespace (see https://github.com/Tiiiger/bert_score?tab=readme-ov-file#practical-tips)
    output_data = [re.sub(r"\s+", " ", o) for o in output_data]

    data = {
        "generation": (input_data[:n_gen], output_data[:n_gen]),
        "evaluation": (input_data[n_gen:], output_data[n_gen:]),
    }

    return data


def generate_prompts(
    conf, base_conf, prompt_gen_template, demos_template, prompt_gen_data
):
    conf = ape.config.update_config(conf, base_conf)
    demos_template = ape.template.DemosTemplate(demos_template)
    prompt_gen_template = ape.template.GenerationTemplate(prompt_gen_template)

    print("Generating prompts...")
    prompts = ape.generate.generate_prompts(
        prompt_gen_template, demos_template, prompt_gen_data, conf["generation"]
    )

    return prompts


def evaluate_prompts(
    prompts,
    eval_template,
    demos_template,
    eval_data,
    conf,
    base_conf,
    few_shot_data=None,
):
    conf = ape.config.update_config(conf, base_conf)
    eval_template = ape.template.EvalTemplate(eval_template)
    demos_template = ape.template.DemosTemplate(demos_template)

    few_shot_data = few_shot_data or []

    res = ape.evaluate.evalute_prompts(
        prompts,
        eval_template,
        eval_data,
        demos_template,
        few_shot_data,
        conf["evaluation"]["method"],
        conf["evaluation"],
    )

    return res


def save_promtps(prompts: list[str], data_dir: Path, model_type: str):
    output_path = data_dir / "ape" / f"raw_prompts_{model_type}.json"

    with open(output_path, "w") as f:
        json.dump(prompts, f, indent=4)


def load_promtps(data_dir: Path, model_type: str):
    prompt_path = data_dir / "ape" / f"prompts_{model_type}.json"

    with open(prompt_path) as f:
        prompts = json.load(f)

    return prompts


def save_results(
    results: ape.evaluate.EvaluationResult, data_dir: Path, model_type: str
):
    output_path = data_dir / "ape" / f"results_{model_type}.json"

    df_results = pd.DataFrame(results.sorted(), index=["prompt", "score"]).T
    df_results.to_csv(output_path, index=False)
