import argparse
import json
import os
from pathlib import Path
from typing import NamedTuple

import pandas as pd

from datasets import Dataset

from thesis.classification import (
    get_condition,
    apply_data_split,
    ClassificationCondition,
    get_model_dir,
    get_evaluation_dir,
    get_evaluator,
    get_classifier_input,
    ModelType,
    load_model,
    CLASS_LABELS,
)
from thesis.classification.utils import DisableDatasetsProgressBar
from thesis import StrategiesType

EVALUATION_STRATEGIES: list[StrategiesType] = [
    "minimal",
    "few_shot_truthful",
    "few_shot_deceptive",
    "persona_young",
    "persona_old",
    "fake",
    "crowdworkers",
]


class Inputs(NamedTuple):
    approach: str
    cond: ClassificationCondition
    data_dir: Path
    model_dir: Path
    evaluation_dir: Path


def _get_inputs() -> Inputs:
    parser = argparse.ArgumentParser()
    parser.add_argument("task_id", type=int)
    parser.add_argument("approach")
    parser.add_argument("data_dir", type=Path)
    parser.add_argument("models_dir", type=Path)

    args = parser.parse_args()
    cond = get_condition(args.task_id, args.data_dir)

    inputs = Inputs(
        approach=args.approach,
        cond=cond,
        model_dir=get_model_dir(args.models_dir, args.approach, cond),
        data_dir=args.data_dir,
        evaluation_dir=get_evaluation_dir(args.data_dir, args.approach, cond),
    )

    if not inputs.evaluation_dir.exists():
        inputs.evaluation_dir.mkdir(parents=True)

    return inputs


def _load_amazon_data(data_dir: Path) -> Dataset:
    reviews = pd.read_csv(data_dir / "human" / "review_data_preprocessed.csv")
    amazon_reviews = (
        reviews.loc[lambda df: ~df.matched_review_id.isna()]
        .rename(columns={"review_title": "title", "review_text": "text"})
        .assign(
            review_id=lambda df: df.matched_review_id.astype(int),
            text=get_classifier_input,
            label="human",
        )
        .loc[:, ["review_id", "text", "label"]]
    )

    # Transform to HuggingFace Dataset
    with DisableDatasetsProgressBar():
        amazon_data = Dataset.from_pandas(
            amazon_reviews, preserve_index=False
        ).cast_column("label", CLASS_LABELS)
    return amazon_data


def _evaluate_model(
    model: ModelType,
    reviews: Dataset,
    cond: ClassificationCondition,
    classification_threshold: float,
) -> pd.DataFrame:
    evaluation_set = apply_data_split(reviews, cond.split)["test"]
    evaluator = get_evaluator(model)
    eval_results = evaluator(model, evaluation_set, classification_threshold)

    return eval_results


def main():
    inputs = _get_inputs()

    # Load data
    amazon_reviews = _load_amazon_data(inputs.data_dir)
    model, classification_threshold = load_model(inputs.model_dir, inputs.approach)

    # Evaluate
    results = _evaluate_model(
        model, amazon_reviews, inputs.cond, classification_threshold
    )
    results.to_csv(inputs.evaluation_dir / "evaluation_results_amazon.csv", index=False)


if __name__ == "__main__":
    main()
