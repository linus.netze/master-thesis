from typing import TypeVar, Type, Any, TypeGuard

_T = TypeVar("_T")


def is_list_of(val: list[Any], type: Type[_T]) -> TypeGuard[list[_T]]:
    """Check if a list contains only elements of a given type. Used for type checking."""
    return all(isinstance(element, type) for element in val)
