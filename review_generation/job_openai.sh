#!/usr/bin/zsh

#SBATCH -J review_generation_openai
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=8G
#SBATCH --time=00:40:00

echo "Starting task $SLURM_ARRAY_TASK_ID"
echo "Account: $SLURM_JOB_ACCOUNT"

echo "Activating conda environment"

# Activate conda environment
export CONDA_ROOT=$HOME/miniconda3
. $CONDA_ROOT/etc/profile.d/conda.sh
export PATH="$CONDA_ROOT/bin:$PATH"
conda activate thesis

echo "Starting python script"

python execute_task.py "$SLURM_ARRAY_TASK_ID" "openai" --tasks_dir "$TASKS_DIR/openai_tasks" --data_dir "$DATA_DIR"