import argparse
import json
import os
from pathlib import Path
from typing import NamedTuple

import pandas as pd

import datasets
from datasets import Dataset, DatasetDict

from thesis.classification import (
    get_condition_dataset,
    get_condition,
    load_data,
    get_trainer,
    ReviewSets,
    apply_data_split,
    filter_by_strategies,
    ClassificationCondition,
    get_model_dir,
    get_evaluation_dir,
    get_evaluator,
    ModelType,
)
from thesis import StrategiesType

EVALUATION_STRATEGIES: list[StrategiesType] = [
    "minimal",
    "few_shot_truthful",
    "few_shot_deceptive",
    "persona_young",
    "persona_old",
    "fake",
    "crowdworkers",
]


class Inputs(NamedTuple):
    approach: str
    cond: ClassificationCondition
    data_dir: Path
    model_dir: Path
    evaluation_dir: Path


def _get_inputs() -> Inputs:
    parser = argparse.ArgumentParser()
    parser.add_argument("task_id", type=int)
    parser.add_argument("approach")
    parser.add_argument("data_dir", type=Path)
    parser.add_argument("models_dir", type=Path)

    args = parser.parse_args()
    cond = get_condition(args.task_id, args.data_dir)

    inputs = Inputs(
        approach=args.approach,
        cond=cond,
        model_dir=get_model_dir(args.models_dir, args.approach, cond),
        data_dir=args.data_dir,
        evaluation_dir=get_evaluation_dir(args.data_dir, args.approach, cond),
    )

    if inputs.model_dir.exists():
        raise ValueError(f"Model directory {inputs.model_dir} already exists")

    if not inputs.evaluation_dir.exists():
        inputs.evaluation_dir.mkdir(parents=True)

    return inputs


def _train_model(
    reviews: ReviewSets, approach: str, cond: ClassificationCondition, output_dir: Path
) -> tuple[ModelType, float]:
    task_data = get_condition_dataset(reviews, cond)
    trainer = get_trainer(approach, task_data, output_dir, cond)
    trainer.train()

    model = trainer.model
    classification_threshold = trainer.classification_threshold

    return model, classification_threshold


def _save_training_results(
    output_dir: Path, model: ModelType, classification_threshold: float
):
    model.save_pretrained(output_dir)
    with open(output_dir / "inference_config.json", "w") as f:
        json.dump({"classification_threshold": classification_threshold}, f)


def _get_evaluation_dataset(
    reviews: ReviewSets,
    split: dict[str, list[int]],
) -> Dataset:
    filtered_reviews = filter_by_strategies(reviews.llm, EVALUATION_STRATEGIES)
    data = datasets.concatenate_datasets([reviews.human, filtered_reviews])
    evaluation_set = apply_data_split(data, split)["test"]

    return evaluation_set


def _evaluate_model(
    model: ModelType,
    reviews: ReviewSets,
    cond: ClassificationCondition,
    classification_threshold: float,
) -> pd.DataFrame:
    evaluation_set = _get_evaluation_dataset(reviews, cond.split)
    evaluator = get_evaluator(model)
    eval_results = evaluator(model, evaluation_set, classification_threshold)

    return eval_results


def main():
    inputs = _get_inputs()

    # Load data
    reviews = load_data(inputs.data_dir)

    # Train model
    model, classification_threshold = _train_model(
        reviews, inputs.approach, inputs.cond, inputs.model_dir
    )
    _save_training_results(inputs.model_dir, model, classification_threshold)

    # Evaluate
    results = _evaluate_model(model, reviews, inputs.cond, classification_threshold)
    results.to_csv(inputs.evaluation_dir / "evaluation_results.csv", index=False)


if __name__ == "__main__":
    main()
