#!/usr/bin/zsh

#SBATCH -J amazon_inference
#SBATCH --gres=gpu:1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=48G
#SBATCH --time=00:20:00

APPROACH="bert"
export CONDA_ROOT=$HOME/miniconda3

echo "Account: $SLURM_JOB_ACCOUNT"

if [[ -z "${SLURM_JOB_ID}" ]]; then
    export SLURM_ARRAY_TASK_ID=1
fi

# Activate conda environment
echo "Activating conda environment"
. $CONDA_ROOT/etc/profile.d/conda.sh
export PATH="$CONDA_ROOT/bin:$PATH"
conda activate thesis

echo "Starting python script"

python ./amazon_inference.py "$SLURM_ARRAY_TASK_ID" "$APPROACH" "$DATA_DIR" "$MODELS_DIR"
