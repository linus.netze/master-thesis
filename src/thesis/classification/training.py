from pathlib import Path
from typing import Any
from abc import ABC, abstractmethod
from shutil import rmtree

import pandas as pd
import torch

from sklearn.linear_model import LogisticRegression

import wandb

import transformers
from transformers import (
    AutoTokenizer,
    AutoModelForSequenceClassification,
    TrainingArguments,
    Trainer,
    DataCollatorWithPadding,
    EvalPrediction,
)
import setfit
from setfit import SetFitModel
from datasets import Dataset, DatasetDict

from scipy.special import softmax

from .evaluation import compute_accuracy, get_youdens_j
from .data import CLASS_LABELS
from .classification import ClassificationCondition


import numpy as np

SEED = 42
MODEL = "roberta-large"


class BaseTrainer(ABC):
    _trainer: Trainer | setfit.Trainer
    DEFAULT_TRAIN_CONFIG: dict[str, Any]
    DEFAULT_MODEL: str
    _WANDB_PROJECT: str

    def __init__(self, output_dir: Path, cond: ClassificationCondition):
        transformers.set_seed(SEED)
        self._classification_threshold = None
        self._cond = cond
        self._output_dir = output_dir

        wandb.init(
            project=self._WANDB_PROJECT,
            name=self._wandb_run_name,
            group=self._wandb_group_name,
            config=cond._asdict(),
        )

    def train(self):
        self._trainer.train()
        self._classification_threshold = self._calculate_classification_threshold()
        self._remove_checkpoints()

    @property
    def model(self):
        return self._trainer.model

    @property
    def classification_threshold(self) -> float:
        if self._classification_threshold is None:
            raise Exception("Classification threshold not set. Train model first.")

        return self._classification_threshold

    @abstractmethod
    def evaluate() -> dict[str, Any]:
        raise NotImplementedError

    def _calculate_classification_threshold(self) -> float:
        eval_results = self.evaluate()
        classification_threshold = eval_results["eval_classification_threshold"]

        return classification_threshold

    @property
    def _wandb_group_name(self) -> str:
        group_name = f"{self._cond.veracity}_{self._cond.source}_{self._cond.strategy}"

        return group_name

    @property
    def _wandb_run_name(self) -> str:
        group_name = self._wandb_group_name
        run_name = f"{group_name}_{self._cond.split_id}"

        return run_name

    @property
    def _wandb_condition_config(self):
        condition_config = {
            "veracity": self._cond.veracity,
            "source": self._cond.source,
            "strategy": self._cond.strategy,
            "split_id": self._cond.split_id,
        }

        return condition_config

    def _remove_checkpoints(self):
        for d in self._output_dir.iterdir():
            if d.is_dir() and d.stem.startswith("checkpoint"):
                rmtree(d)


def get_trainer(
    approach: str,
    dataset: DatasetDict,
    output_dir: Path,
    cond: ClassificationCondition,
    model_name: str | None = None,
    train_config: dict[str, Any] | None = None,
) -> BaseTrainer:
    if approach == "setfit":
        trainer_cls = SetFitTrainer
    elif approach == "bert":
        trainer_cls = BertTrainer
    else:
        raise Exception(f"Unknown approach '{approach}'.")

    trainer = trainer_cls(dataset, output_dir, cond, model_name, train_config)

    return trainer


class SetFitTrainer(BaseTrainer):
    DEFAULT_TRAIN_CONFIG = {
        "batch_size": 8,
        "num_epochs": 1,
        "max_steps": 500,
        "sampling_strategy": "undersampling",
        "evaluation_strategy": "steps",
        "save_strategy": "steps",
        "load_best_model_at_end": True,
        "report_to": "wandb",
    }
    DEFAULT_MODEL = "sentence-transformers/all-roberta-large-v1"
    _WANDB_PROJECT = "ReviewClassificationSetFit"

    def __init__(
        self,
        dataset: DatasetDict,
        output_dir: Path,
        cond: ClassificationCondition,
        model_name: str | None = None,
        train_config: dict[str, Any] | None = None,
    ):
        super().__init__(output_dir, cond)

        train_config = train_config or self.DEFAULT_TRAIN_CONFIG
        model_name = model_name or self.DEFAULT_MODEL

        # Load a SetFit model from Hub
        self._model = SetFitModel.from_pretrained(
            model_name, use_differentiable_head=False
        )

        self._args = setfit.TrainingArguments(
            output_dir=str(output_dir),
            **train_config,
        )

        self._dataset = dataset

        self._model.to("cuda")
        self._trainer = setfit.Trainer(
            model=self._model,
            args=self._args,
            train_dataset=dataset["train"],
            eval_dataset=dataset["valid"],
        )

    def evaluate(self) -> dict[str, Any]:
        eval_data = self._dataset["valid"]
        texts = eval_data["text"]
        labels = eval_data["label"]

        assert isinstance(self.model.model_head, LogisticRegression)
        pos_idx = list(self.model.model_head.classes_).index(1)
        scores = self.model.predict_proba(texts)[:, pos_idx]

        threshold = get_youdens_j(eval_data["label"], scores)

        metrics = {"eval_classification_threshold": threshold}

        accuracy = compute_accuracy(labels, scores, 0.5)
        metrics |= accuracy

        accuracy_youden = compute_accuracy(labels, scores, threshold)
        metrics |= {f"{k}_youden": v for k, v in accuracy_youden.items()}

        return metrics


class BertTrainer(BaseTrainer):
    DEFAULT_TRAIN_CONFIG = {
        "per_device_train_batch_size": 4,
        "per_device_eval_batch_size": 4,
        "warmup_ratio": 0.1,
        "learning_rate": 2e-5,
        "gradient_accumulation_steps": 8,
        "num_train_epochs": 10,
        "fp16": True,
        "report_to": "wandb",
        "evaluation_strategy": "epoch",
        "save_strategy": "epoch",
        "logging_steps": 10,
        "load_best_model_at_end": True,
        "save_total_limit": 1,
        "metric_for_best_model": "eval_loss",
        "greater_is_better": False,
    }
    DEFAULT_MODEL = "roberta-large"
    _WANDB_PROJECT = "ReviewClassificationBert"

    def __init__(
        self,
        dataset: DatasetDict,
        output_dir: Path,
        cond: ClassificationCondition,
        model_name: str | None = None,
        train_config: dict[str, Any] | None = None,
    ):
        super().__init__(output_dir, cond)

        train_config = train_config or self.DEFAULT_TRAIN_CONFIG
        model_name = model_name or self.DEFAULT_MODEL

        self._tokenizer = AutoTokenizer.from_pretrained(model_name)
        data_collator = DataCollatorWithPadding(tokenizer=self._tokenizer)
        model = AutoModelForSequenceClassification.from_pretrained(
            model_name,
            num_labels=2,
            id2label={i: l for i, l in enumerate(CLASS_LABELS._int2str)},
            label2id=CLASS_LABELS._str2int,
        )

        training_args = TrainingArguments(
            output_dir=str(output_dir),
            **train_config,
        )

        dataset_tokenized = dataset.map(self._tokenize, batched=True)

        self._trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=dataset_tokenized["train"],
            eval_dataset=dataset_tokenized["valid"],
            tokenizer=self._tokenizer,
            data_collator=data_collator,
            compute_metrics=self._metric,
        )

    def _tokenize(self, data: Dataset):
        return self._tokenizer(data["text"], truncation=True, padding=False)

    def _metric(self, eval_pred: EvalPrediction) -> dict:
        logits, labels = eval_pred
        assert isinstance(logits, np.ndarray)
        assert isinstance(labels, np.ndarray)
        probabilities = softmax(logits, axis=1)
        scores = probabilities[:, 1]

        metrics = dict()
        metrics["classification_threshold"] = get_youdens_j(labels, scores)

        accuracy = compute_accuracy(labels, scores, 0.5)
        metrics |= accuracy

        accuracy_youden = compute_accuracy(
            labels, scores, metrics["classification_threshold"]
        )
        metrics |= {f"{k}_youden": v for k, v in accuracy_youden.items()}

        return metrics

    def evaluate(self) -> dict[str, Any]:
        return self._trainer.evaluate()
