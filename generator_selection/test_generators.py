import json
import torch
import argparse
import transformers
from transformers import (
    pipeline,
    Pipeline,
    AutoTokenizer,
    GenerationConfig,
)
import tomllib

from time import perf_counter
from collections.abc import Mapping
from pathlib import Path
from os import PathLike

from src.config import Config

from src.prompts import PromptFactory, CUSTOM_CHAT_TEMPLATE_MAP, CHAT_TEMPLATES
from src.utils import is_list_of

import torch

DEFAULT_RESULTS_DIR = "./results"

SEED = 42
transformers.enable_full_determinism(seed=SEED)


def _get_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("model_id")
    parser.add_argument("setting_name")
    parser.add_argument("--results_dir", default=DEFAULT_RESULTS_DIR, type=Path)

    args = parser.parse_args()

    if not args.results_dir.exists():
        raise Exception(f"Directory '{args.results_dir}' not found.")

    return args


def _generate_texts(config: Config) -> dict[str, str]:
    pipe = _setup_pipeline(config.model_id)

    # Generate prompts
    assert pipe.tokenizer
    prompt_factory = PromptFactory(
        config.model_id, pipe.tokenizer, config.prompt_template_path
    )
    prompt_configs = _load_prompt_configs(config.prompt_config_path)

    generated_texts = dict()
    for task, prompt_config in prompt_configs.items():
        print(f"Generate '{task}'.")
        prompt = prompt_factory(prompt_config)
        texts = _generate(prompt, pipe, config, SEED)

        generated_texts[task] = {"prompt": prompt, "text": texts}

    return generated_texts


def _generate(
    prompt: str, pipe: Pipeline, config: Config, seed: int
) -> str | list[str]:
    model_id = pipe.model.name_or_path
    generation_config = GenerationConfig.from_pretrained(
        model_id, **config.generation.dict()
    )

    transformers.set_seed(seed)
    start = perf_counter()
    outputs = pipe(prompt, return_full_text=False, generation_config=generation_config)
    end = perf_counter()

    assert isinstance(outputs, list)
    assert is_list_of(outputs, Mapping)

    print(f"    generation time: {end-start}")

    if len(outputs) == 1:
        return outputs[0]["generated_text"]

    return [output["generated_text"] for output in outputs]


def _setup_pipeline(model_id: str) -> Pipeline:
    # Tokenizer
    tokenizer_kwargs = dict()
    if custom_chat_template := CUSTOM_CHAT_TEMPLATE_MAP.get(model_id):
        tokenizer_kwargs["chat_template"] = CHAT_TEMPLATES[custom_chat_template]

    tokenizer = AutoTokenizer.from_pretrained(
        model_id, use_default_system_prompt=False, **tokenizer_kwargs
    )

    # Pipeline
    pipe = pipeline(
        "text-generation",
        model=model_id,
        tokenizer=tokenizer,
        torch_dtype=torch.bfloat16,
        device_map="auto",
    )

    return pipe


def _load_prompt_configs(path: str | PathLike) -> dict[str, dict]:
    with open(path, "rb") as f:
        prompt_configs = tomllib.load(f)

    defaults = prompt_configs.pop("default")
    prompt_configs = {
        task: defaults | config for task, config in prompt_configs.items()
    }

    return prompt_configs


def _save_results(texts: Mapping[str, str], config: Config):
    print("Save results")

    if not config.output_path.parent.exists():
        config.output_path.parent.mkdir(parents=True)

    with open(config.output_path, "w") as f:
        json.dump(texts, f, indent=4)


def _check_setting_exists(config: Config) -> None:
    if config.output_path.exists():
        raise ValueError(
            f"Output for setting '{config.setting_name}' already exists at '{config.path}'."
        )


def main():
    args = _get_arguments()
    config = Config.from_cli_args_and_file(args)

    _check_setting_exists(config)

    texts = _generate_texts(config)
    _save_results(texts, config)


if __name__ == "__main__":
    main()
