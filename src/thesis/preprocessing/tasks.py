from pathlib import Path
from typing import Any
from collections import defaultdict
from collections.abc import Generator

import json

import pandas as pd

import numpy as np
from numpy.random import default_rng

from ..generation.generation import SPECIAL_STRATEGY_PARAMS

SEED = 42

BATCH_SIZE = 5
DEFAULT_GEN_TIME: int = 60
SPECIAL_GEN_TIMES = {"few_shot_deceptive": 120, "few_shot_truthful": 120}
MAX_TIME = 45 * 60


def _define_single_batch(
    batch_id: int,
    temperature_range: tuple[float, float],
) -> dict[str, Any]:
    batch = dict()
    batch["id"] = batch_id

    seed = SEED + batch_id
    rng = default_rng(seed)
    batch["seed"] = seed
    batch["temperature"] = rng.uniform(
        low=temperature_range[0], high=temperature_range[1]
    )

    return batch


def define_initial_batches(
    data: pd.DataFrame,
    temperature_range: tuple[float, float],
    strategies: list[str] | None = None,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    # Shuffle data
    data = data.sample(frac=1, ignore_index=False, random_state=SEED)

    # Sort reviews by length to ensure that batches have similar length constraints => higher batch inference efficiency
    data = data.sort_values("length_category")

    if strategies is None:
        strategies = list(SPECIAL_STRATEGY_PARAMS.keys())
    df_strategies = pd.DataFrame({"strategy": strategies})

    review_batch_map = (
        pd.DataFrame(index=data.index)
        .reset_index(drop=False, names="review_id")
        .assign(
            hf_batch_id=lambda df: (np.arange(len(df)) // BATCH_SIZE),
            openai_batch_id=lambda df: (np.arange(len(df)) // BATCH_SIZE),
        )
        .merge(df_strategies, how="cross")
        .sort_values(["strategy", "batch_id"])
        .loc[:, ["strategy", "review_id", "hf_batch_id", "openai_batch_id"]]
    )

    batches_records = [
        _define_single_batch(batch_id, temperature_range)
        for batch_id in review_batch_map["hf_batch_id"].unique()
    ]
    batches = pd.DataFrame.from_records(batches_records).set_index("id")

    return batches, review_batch_map


def move_reviews_to_new_batch(
    reviews: dict[str, list[int]],
    batch_definitions: pd.DataFrame,
    review_batch_map: pd.DataFrame,
    source: str,
) -> tuple[pd.DataFrame, pd.DataFrame]:
    batch_id_column = f"{source}_batch_id"
    review_batch_map = review_batch_map.set_index(["review_id", "strategy"])

    reviews_idx = [
        (review_id, strategy)
        for strategy, review_ids in reviews.items()
        for review_id in review_ids
    ]

    old_batches_idx = review_batch_map.loc[reviews_idx][batch_id_column]
    max_batch_id = batch_definitions.index.max()
    new_batches_idx = np.arange(
        max_batch_id + 1, max_batch_id + 1 + len(old_batches_idx)
    )

    review_batch_map.loc[reviews_idx, batch_id_column] = new_batches_idx

    # Introduce new batches by copying definitions for old batches and assinging new seed
    new_batch_definitions = (
        batch_definitions.loc[old_batches_idx]
        .set_index(pd.Series(new_batches_idx, name="id"))
        .assign(seed=lambda df: df.index + SEED)
    )
    batch_definitions = pd.concat(
        [batch_definitions, new_batch_definitions], verify_integrity=True
    )

    review_batch_map = review_batch_map.reset_index(drop=False).loc[
        :, ["strategy", "review_id", "hf_batch_id", "openai_batch_id"]
    ]

    return batch_definitions, review_batch_map


def define_tasks(
    strategies: list[str],
    batches: pd.DataFrame,
    start_id: int,
):
    tasks = list()
    for strategy in strategies:
        batch_time = SPECIAL_GEN_TIMES.get(strategy, DEFAULT_GEN_TIME)

        current_task = {"strategy": strategy, "batches": list()}
        current_task_time = 0
        for batch_id, batch_time in zip(batches.index, len(batches) * [batch_time]):
            if current_task_time + batch_time > MAX_TIME:
                tasks.append(current_task)
                current_task = {"strategy": strategy, "batches": list()}
                current_task_time = 0

            current_task["batches"].append(batch_id)
            current_task_time += batch_time

        tasks.append(current_task)

    tasks = {i: task for i, task in enumerate(tasks, start=start_id)}

    return tasks


def get_max_task_id(tasks_dir: Path) -> int:
    task_ids = [int(str(d.stem)) for d in tasks_dir.iterdir() if d.is_dir()]

    if len(task_ids) == 0:
        return -1

    return max(task_ids)


def save_tasks(tasks_dir: Path, tasks: dict[int, list[int]]):
    for i, task in tasks.items():
        task_path = tasks_dir / f"{i}"

        if task_path.exists():
            raise FileExistsError(f"Task {i} already exists.")

        task_path.mkdir()

        task_definition_path = task_path / f"definition.json"
        with open(task_definition_path, "w") as f:
            json.dump(task, f, indent=4)
