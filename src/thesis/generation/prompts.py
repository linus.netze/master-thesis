import tomllib
import json
from os import PathLike
from copy import copy
from abc import ABC, abstractmethod
from typing import Any

import pandas as pd
from transformers import PreTrainedTokenizer, PreTrainedTokenizerFast

from thesis.definitions import LENGTH_QUANTILES

NUM_TO_WORD = [
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
]

RATING_TO_WORD = {
    1: "very bad",
    2: "bad",
    3: "neutral",
    4: "good",
    5: "very good",
}

SYSTEM_PROMPTS = {
    # https://arxiv.org/pdf/2307.09288.pdf
    "llama": (
        "You are a helpful, respectful and honest assistant. Always answer as helpfully"
        " as possible, while being safe. Your answers should not include any harmful,"
        " unethical, racist, sexist, toxic, dangerous, or illegal content. Please ensure that"
        " your responses are socially unbiased and positive in nature.\n\n"
        "If a question does not make any sense, or is not factually coherent, explain why"
        " instead of answering something not correct. If you don’t know the answer to a"
        " question, please don’t share false information."
    ),
    # https://github.com/lm-sys/FastChat/blob/main/docs/vicuna_weights_version.md#prompt-template
    "vicuna": (
        "A chat between a curious user and an artificial intelligence assistant. The assistant gives helpful,"
        " detailed, and polite answers to the user's questions."
    ),
}
SYSTEM_PROMPT_MAP = {
    "lmsys/vicuna-13b-v1.5": "vicuna",
    "WizardLM/WizardLM-13B-V1.2": "vicuna",
    "meta-llama/Llama-2-13b-chat-hf": "llama",
    # For GPT-4 use the same system prompt as vicuna for better comparability
    "gpt-4-1106-preview": "vicuna",
}

CHAT_TEMPLATES = {
    # ),
    # Vicuna does not provide their chat template on huggingface hub and does not work well with default llama chat template.
    # Use template from https://huggingface.co/lmsys/vicuna-13b-v1.5/discussions/6, which is based on lmsys'
    # (the company behind vicuna) proposed template:
    # https://github.com/lm-sys/FastChat/blob/main/docs/vicuna_weights_version.md#prompt-template
    "vicuna": (
        "{% if messages[0]['role'] == 'system' %}"
        "{% set loop_messages = messages[1:] %}"
        "{% set system_message = messages[0]['content'] %}"
        "{% else %}"
        "{% set loop_messages = messages %}"
        "{% set system_message = '' %}"
        "{% endif %}"
        "{% for message in loop_messages %}"
        "{% if (message['role'] == 'user') != (loop.index0 % 2 == 0) %}"
        "{{ raise_exception('Conversation roles must alternate user/assistant/user/assistant/...') }}"
        "{% endif %}{% if loop.index0 == 0 %}{{ system_message }}{% endif %}{% if message['role'] == 'user' %}"
        "{{ ' USER: ' + message['content'].strip() }}{% elif message['role'] == 'assistant' %}"
        "{{ ' ASSISTANT: ' + message['content'].strip() + eos_token }}"
        "{% endif %}{% endfor %}{% if add_generation_prompt %}{{ ' ASSISTANT:' }}{% endif %}"
    ),
}
CUSTOM_CHAT_TEMPLATE_MAP = {
    "lmsys/vicuna-13b-v1.5": "vicuna",
    "WizardLM/WizardLM-13B-V1.2": "vicuna",
}

INSTRUCTION_TEMPLATES = {
    "instruction": {
        "base": "{persona}Write {n_reviews} {rating}/5-star ({rating_name}) customer {review_plural} including a title for the smartphone {model}. {each_the} review should be {length_constraint} long. Return the {review_plural} in {format}",
        "crowdworkers": (
            "For the following question, you will be asked to write reviews about smartphones. In the case of the rating, a five-point scale will be used as follows:\n"
            "- 1 star = very bad\n"
            "- 2 stars = bad\n"
            "- 3 stars = neutral\n"
            "- 4 stars = good\n"
            "- 5 stars = very good\n"
            "\n"
            "Please write {n_reviews} {review_plural} with a title about the smartphone {model} that corresponds to a {rating}-star ({rating_name}) rating. Each review should be {length_constraint} long. Return the {review_plural} in {format}"
        ),
    },
    "format": {
        "json_array": (
            "a JSON array with the following format:\n"
            "\n"
            '[{"title": <title>, "text": <reviewtext>}, ...]'
        ),
        "json_object": (
            "a JSON object with the following format:\n"
            "\n"
            '{"title": <title>, "text": <reviewtext>}'
        ),
    },
}
PERSONA_PROMPTS = {
    "tech_enthusiast": "As the assistant you pretend to be a 25 years old tech enthusiast.",
    "older_adult": "As the assistant you pretend to be 55 years old and to have only a basic understanding of smartphones.",
    "fake": "As the assistant you pretend to be a crowd worker whose task it is to write fake reviews for products you do not own.",
}


class PromptFactoryBase(ABC):
    _templates: dict[str, Any]
    _length_quantiles: list[int]

    def __init__(self, model_id: str):
        system_prompt_type = SYSTEM_PROMPT_MAP.get(model_id)
        self._system_prompt = (
            SYSTEM_PROMPTS[system_prompt_type] if system_prompt_type else None
        )

    def _get_instruction(
        self,
        instruction_type: str,
        model: str,
        rating: int,
        length: int,
        length_as_category: bool = True,
        n_reviews: int = 1,
        persona: str | None = None,
    ) -> str:
        if rating > 5 or rating < 1:
            raise ValueError(f"Rating must be between 1 and 5, got {rating}.")

        instruction_template = self._templates["instruction"][instruction_type]

        persona_text = (
            f"{self._templates['persona'][persona].strip()} " if persona else ""
        )

        if n_reviews == 1:
            output_format = "json_object"
            review_plural = "review"
            each_the = "The"
            n_reviews_text = "a"
        else:
            output_format = "json_array"
            review_plural = "reviews"
            each_the = "Each"
            n_reviews_text = NUM_TO_WORD[n_reviews]

        length_constraint = self._get_length_constraint_string(
            length, length_as_category
        )

        format_text = self._templates["format"][output_format]

        prompt = instruction_template.format(
            persona=persona_text,
            model=model,
            rating=rating,
            rating_name=RATING_TO_WORD[rating],
            n_words=length,
            n_reviews=n_reviews_text,
            review_plural=review_plural,
            length_constraint=length_constraint,
            format=format_text,
            each_the=each_the,
        )

        return prompt

    def __call__(
        self, instruction_args: dict, examples: list[dict] | None
    ) -> list[dict[str, str]]:
        instruction_args = copy(instruction_args)
        persona = instruction_args.pop("persona", None)

        messages = list()
        if self._system_prompt:
            system_prompt = self._get_system_prompt(persona)
            messages.append({"role": "system", "content": system_prompt})
        elif persona:
            raise ValueError(
                f"System prompt is not defined, but persona is set to {persona}."
            )

        if examples:
            messages.extend(self._get_example_messages(examples))

        instruction = self._get_instruction(**instruction_args)
        messages.append({"role": "user", "content": instruction})

        return messages

    def _get_system_prompt(self, persona: str | None) -> str:
        if self._system_prompt is None:
            raise ValueError(f"System prompt is not defined.")

        if persona is None:
            return self._system_prompt

        persona_prompt = PERSONA_PROMPTS[persona]

        system_prompt = f"{self._system_prompt} {persona_prompt}"

        return system_prompt

    def _get_length_constraint_string(self, length, length_as_category):
        if not length_as_category:
            length_constraint = f"around {length} words"
            return length_constraint

        if not (length < len(self._length_quantiles) - 1):
            raise ValueError(
                f"When length_as_category is True, length must be less than {len(self._length_quantiles)-1}, got {length}."
            )

        length_constraint = f"between {self._length_quantiles[length]} and {self._length_quantiles[length+1]} words"

        return length_constraint

    def _get_example_messages(self, examples: list[dict]) -> list[dict[str, str]]:
        example_messages = list()
        for example in examples:
            instruction = self._get_instruction(
                instruction_type="base",
                model=example["model"],
                rating=example["rating"],
                length=example["length"],
                length_as_category=True,
            )
            example_messages.append({"role": "user", "content": instruction})

            answer = json.dumps(
                {
                    "title": example["title"],
                    "text": example["text"],
                }
            )
            example_messages.append({"role": "assistant", "content": answer})

        return example_messages


class PromptFactory(PromptFactoryBase):
    _templates = INSTRUCTION_TEMPLATES
    _length_quantiles = LENGTH_QUANTILES

    def __call__(self, prompt_params: pd.Series) -> str:
        params = prompt_params.to_dict()
        examples = params.pop("examples", None)
        if ("persona" in params) and (pd.isna(params["persona"])):
            params["persona"] = None

        return super().__call__(params, examples)


class PromptFactoryGeneratorSelection(PromptFactoryBase):
    # Length (in number of words) quantiles of the human review data
    _length_quantiles = [7, 23, 34, 47, 67, 343]

    EXAMPLES = [
        {
            "model": "Apple iPhone 8",
            "rating": 4,
            "title": "Really good iphone",
            "text": "although not advertised as waterproof, have dropped my phone in the toilet many times and is still working. is like every other iPhone, battery life is slightly questionable after every update but overall a good phone",
            "length": 2,
        },
        {
            "model": "Samsung Galaxy A5",
            "rating": 3,
            "title": "Galaxy A5",
            "text": "The phone was great at first, it had a good camera and work very well but after some time the battery didn't last long, and WhatsApp didn't work but this happened like 4 years after I bought it.",
            "length": 2,
        },
        {
            "model": "Apple iPhone 6",
            "rating": 4,
            "title": "iphone 6",
            "text": "Everything worked fine, but after the updates, the phone was glitching more often. Also, after a few hits, it stopped working properly while others phones I had were more stable. It is also more expensive to fix Iphones.",
            "length": 2,
        },
        {
            "model": "Samsung Galaxy Grand Prime",
            "rating": 3,
            "title": "Slow smartphone",
            "text": "This smartphone works fine for a couple months but over time it becomes very slow, it gets very hot, it crashes in some apps and games, the battery lasts very little and its storage space is very little.",
            "length": 2,
        },
        {
            "model": "Apple iPhone 3G",
            "rating": 3,
            "title": "Os too big for the phone system",
            "text": "The phone itself is great but as we are talking about a very old model (I-Phone 3) the OS is way too big and heavy for the smartphone; that being said the phone is very slow and sometimes the screen frozes.",
            "length": 2,
        },
    ]

    def __init__(
        self,
        model_id: str,
        tokenizer: PreTrainedTokenizer | PreTrainedTokenizerFast,
        template_path: str | PathLike,
    ):
        with open(template_path, "rb") as f:
            self._templates = tomllib.load(f)

        super().__init__(model_id, tokenizer)

    def __call__(self, instruction_args: dict) -> str:
        use_examples = instruction_args.pop("use_examples", False)
        examples = self.EXAMPLES if use_examples else None

        return super().__call__(instruction_args, examples)
