import json
import warnings
import asyncio

from pathlib import Path
from abc import ABC, abstractmethod
from collections.abc import Generator, Mapping
from typing import Any
from itertools import chain
from functools import partial

import pandas as pd
import numpy as np

import torch
from torch.cuda import OutOfMemoryError

from tqdm import tqdm

import transformers
from transformers import (
    pipeline,
    Pipeline,
    AutoTokenizer,
    GenerationConfig,
    Conversation,
)

from openai import AsyncOpenAI

from .prompts import PromptFactory, CUSTOM_CHAT_TEMPLATE_MAP, CHAT_TEMPLATES
from ..utils import is_list_of, is_dict_of
from .. import StrategiesType

HF_MODEL = "WizardLM/WizardLM-13B-V1.2"
GPT_MODEL = "gpt-4-1106-preview"

HUMAN_REVIEWS_FILENAME = "review_data_preprocessed.csv"
TASK_COMPLETE_FILENAME = "TASK_COMPLETE"

EXAMPLE_COLUMN_PREFIXES = {
    "few_shot_deceptive": "example_deceptive",
    "few_shot_truthful": "example_truthful",
}
EXAMPLE_NAMES = ["brand_0", "rating_0", "brand_1", "rating_1", "exact"]

SPECIAL_STRATEGY_PARAMS: dict[StrategiesType, dict] = {
    "minimal": dict(),
    "few_shot_truthful": dict(),
    "few_shot_deceptive": dict(),
    "persona_young": {"persona": "tech_enthusiast"},
    "persona_old": {"persona": "older_adult"},
    "fake": {"persona": "fake"},
    "crowdworkers": {"instruction_type": "crowdworkers"},
    "ape": {"instruction_type": "ape"},
}

GENERATION_CONFIG = {
    "do_sample": True,
    "max_new_tokens": 1024,
    "top_p": 1,
    "top_k": 0,
    "num_return_sequences": 1,
}


class TaskGenerator(ABC):
    SOURCE: str
    MODEL: str

    def __init__(self, task_id: int, tasks_dir: Path, data_dir: Path):
        self._task_path = tasks_dir / f"{task_id}"
        if not self._task_path.exists():
            raise Exception(f"Task '{task_id}' not found.")

        task_complete_path = self._task_path / TASK_COMPLETE_FILENAME
        if task_complete_path.exists():
            raise Exception(f"Task '{task_id}' already completed.")

        print("Loading data...")
        task_definition = self._load_definition(self._task_path)
        self._strategy = task_definition["strategy"]
        self._task_batch_ids = task_definition["batches"]

        (
            self._reviews,
            self._batch_definitions,
            self._review_batch_map,
        ) = self._load_data(data_dir)

        self._examples = self._prepare_examples()
        self._prompts = self._prepare_review_prompts()

    def generate_task(self):
        transformers.enable_full_determinism(seed=42)

        review_batches = self._get_task_batches()
        results = list()
        errors = dict()

        print("Start generating...")
        for i, (batch_id, batch) in tqdm(
            enumerate(review_batches), total=len(self._task_batch_ids)
        ):
            batch_config = self._batch_definitions[batch_id]
            prompts = batch["prompt"].to_list()

            try:
                output = self._generate(prompts, **batch_config)
            except OutOfMemoryError as e:
                errors[batch_id] = str(e)
                self._save_errors(errors)
                continue

            batch_results = batch.assign(output=output, batch_id=batch_id).drop(
                "prompt", axis=1
            )
            results.append(batch_results)

            if (i + 1) % 5 == 0:
                self._save_results(results)
                results = list()

        self._save_results(results)

        task_complete_path = self._task_path / TASK_COMPLETE_FILENAME
        task_complete_path.touch()

    def _prepare_examples(self):
        if not self._strategy in EXAMPLE_COLUMN_PREFIXES:
            return dict()

        example_ids = self._extract_example_ids(self._strategy)

        examples = (
            self._reviews.loc[example_ids]
            .apply(
                get_prompt_parameters,
                axis=1,
                result_type="expand",
                is_example=True,
                strategy=None,
            )
            .to_dict(orient="index")
        )

        return examples

    def _extract_example_ids(self, strategy: str) -> list[int]:
        example_column_prefix = EXAMPLE_COLUMN_PREFIXES[strategy]

        reviews_with_examples = self._review_batch_map.review_id.to_list()

        examples = (
            self._reviews.loc[
                reviews_with_examples,
                lambda df: df.columns.str.startswith(example_column_prefix),
            ]
            .to_numpy("int")
            .flatten()
        )
        examples = np.unique(examples).tolist()

        return examples

    def _prepare_review_prompts(self) -> pd.DataFrame:
        task_reviews = self._review_batch_map.join(
            self._reviews, on="review_id", how="left"
        )

        prompt_factory = PromptFactory(HF_MODEL)

        review_parameters = task_reviews.apply(
            get_prompt_parameters,
            axis=1,
            result_type="expand",
            examples=self._examples,
            strategy=self._strategy,
        )

        prompts = (
            review_parameters.apply(prompt_factory, axis=1)
            .rename("prompt")
            .to_frame()
            .join(self._review_batch_map, how="right")
        )

        return prompts

    def _get_task_batches(
        self,
    ) -> Generator[tuple[int, pd.DataFrame], None, None]:
        prompt_batches = self._prompts.groupby("batch_id")

        for batch_id, batch in prompt_batches:
            batch = batch.sort_values("review_id")
            yield batch_id, batch

    def _load_data(
        self, data_dir: Path
    ) -> tuple[pd.DataFrame, dict[int, dict[str, Any]], pd.DataFrame]:
        review_path = data_dir / HUMAN_REVIEWS_FILENAME
        reviews = pd.read_csv(review_path, index_col="id", dtype={})

        review_batch_map_path = data_dir / "review_batch_map.csv"
        batch_id_column = f"{self.SOURCE}_batch_id"
        review_batch_map = (
            pd.read_csv(review_batch_map_path)
            .assign(batch_id=lambda df: df[batch_id_column])
            # Keep only mappings for current batches and strategy
            .loc[
                lambda df: (df.batch_id.isin(self._task_batch_ids))
                & (df.strategy == self._strategy)
            ]
            .drop(["strategy", "hf_batch_id", "openai_batch_id"], axis=1)
        )

        batches_path = data_dir / "batch_definitions.csv"
        batches = pd.read_csv(batches_path, index_col="id").to_dict(orient="index")

        assert is_dict_of(batches, int, Any)

        return reviews, batches, review_batch_map

    def _load_definition(self, task_path: Path) -> dict[str, Any]:
        definition_path = task_path / "definition.json"
        with open(definition_path, "r") as f:
            definition = json.load(f)

        return definition

    def _save_results(self, results: list[pd.DataFrame]):
        if len(results) == 0:
            return

        df_results = pd.concat(results).assign(strategy=self._strategy)

        results_path = self._task_path / "results.jsonl"
        print_header = not results_path.exists()
        df_results.to_json(
            results_path, mode="a", index=False, orient="records", lines=True
        )

    def _save_errors(self, errors: dict[int, str]):
        errors_path = self._task_path / "errors.json"

        with open(errors_path, "w") as f:
            json.dump(errors, f, indent=4)

    @abstractmethod
    def _generate(
        self, prompt: list[str], pipe: Pipeline, temperature: float, seed: int
    ):
        raise NotImplementedError


class HuggingFaceTaskGenerator(TaskGenerator):
    MODEL = HF_MODEL
    SOURCE = "hf"

    def __init__(
        self,
        task_id: int,
        tasks_dir: Path,
        data_dir: Path,
        ignore_batches: bool = False,
    ):
        super().__init__(task_id=task_id, tasks_dir=tasks_dir, data_dir=data_dir)

        print("Setting up pipeline...")
        self._ignore_batches = ignore_batches
        self._pipeline = _setup_pipeline(HF_MODEL)

    def _generate(
        self, prompt: list[str], temperature: float, seed: int
    ) -> str | list[str]:
        model_id = self._pipeline.model.name_or_path
        generation_config = GenerationConfig.from_pretrained(
            model_id, temperature=temperature, **GENERATION_CONFIG
        )

        batch_size = len(prompt) if not self._ignore_batches else 1

        transformers.set_seed(seed)
        try:
            outputs = self._pipeline(
                prompt,
                generation_config=generation_config,
                batch_size=batch_size,
                max_new_tokens=generation_config.max_new_tokens,
            )
        except OutOfMemoryError as e:
            if self._ignore_batches:
                raise e

            # Try again without batching
            torch.cuda.empty_cache()
            transformers.set_seed(seed)
            outputs = self._pipeline(
                prompt,
                generation_config=generation_config,
                batch_size=1,
                max_new_tokens=generation_config.max_new_tokens,
            )

        if isinstance(outputs, Conversation):
            outputs = [outputs]

        assert isinstance(outputs, list)
        assert is_list_of(outputs, Conversation)

        generated_texts = [output.messages[-1]["content"] for output in outputs]

        return generated_texts


class OpenAiTaskGenerator(TaskGenerator):
    MODEL = GPT_MODEL
    SOURCE = "openai"

    def __init__(self, task_id: int, tasks_dir: Path, data_dir: Path):
        super().__init__(task_id=task_id, tasks_dir=tasks_dir, data_dir=data_dir)

        print("Setting up client...")
        self._client = AsyncOpenAI()

    def _generate(
        self,
        batch: list[list[dict[str, str]]],
        temperature: float,
        seed: int,
    ) -> list[str]:
        async def openai_batched():
            api_call = partial(
                self._client.chat.completions.create,
                model=GPT_MODEL,
                temperature=temperature,
                seed=seed,
                response_format={"type": "json_object"},
            )

            output = await asyncio.gather(
                *[api_call(messages=messages) for messages in batch]
            )

            return output

        outputs = asyncio.run(openai_batched())

        generated_texts = [output.choices[0].message.content for output in outputs]
        assert is_list_of(generated_texts, str)

        return generated_texts


def _setup_pipeline(model_id: str) -> Pipeline:
    # Tokenizer
    tokenizer_kwargs = {"padding_side": "left"}
    if custom_chat_template := CUSTOM_CHAT_TEMPLATE_MAP.get(model_id):
        tokenizer_kwargs["chat_template"] = CHAT_TEMPLATES[custom_chat_template]

    tokenizer = AutoTokenizer.from_pretrained(
        model_id, use_default_system_prompt=False, **tokenizer_kwargs
    )

    # Pipeline
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore",
            category=UserWarning,
            message=r"`do_sample` is set to `False`\. However, `(?:temperature|top_p)` is set to `\d\.\d` -- this flag is only used in sample-based generation modes\. You should set `do_sample=True` or unset `(?:temperature|top_p)`\. This was detected when initializing the generation config instance, which means the corresponding file may hold incorrect parameterization and should be fixed\.",
        )

        pipe = pipeline(
            "conversational",
            model=model_id,
            tokenizer=tokenizer,
            torch_dtype=torch.bfloat16,
            device_map="auto",
        )

    return pipe


def get_prompt_parameters(
    review: pd.Series,
    strategy: str | None,
    examples: dict[int, dict[str, Any]] | None = None,
    is_example: bool = False,
):
    params = dict()
    params["model"] = f"{review.brand} {review.phone_type}"
    params["rating"] = review.review_rating
    params["length"] = int(review.length_category)

    if is_example:
        params["text"] = review.review_text
        params["title"] = review.review_title

        return params

    if strategy is None:
        raise ValueError(
            "Strategy is only allowed to be `None` if `is_example` is `True`."
        )

    params |= get_strategy_params(strategy=strategy)

    if strategy in EXAMPLE_COLUMN_PREFIXES:
        params |= get_example_param(review, strategy, examples)

    return params


def get_strategy_params(strategy):
    params = dict()
    params["examples"] = None
    params["instruction_type"] = "base"

    params |= SPECIAL_STRATEGY_PARAMS[strategy]

    return params


def get_example_param(
    review: pd.Series, strategy: str, examples: dict[int, dict[str, Any]] | None
) -> dict[str, list[dict[str, Any]]]:
    if examples is None:
        raise ValueError(f"Strategy '{strategy}' requires examples to be passed.")

    example_columns = [
        f"{EXAMPLE_COLUMN_PREFIXES[strategy]}_{name}" for name in EXAMPLE_NAMES
    ]
    review_example_ids = review.loc[example_columns].astype("int").to_list()
    review_examples = [examples[example_id] for example_id in review_example_ids]

    return {"examples": review_examples}
