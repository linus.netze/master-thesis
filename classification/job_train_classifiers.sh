#!/usr/bin/zsh

#SBATCH -J classifier_training
#SBATCH --gres=gpu:1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=48G
#SBATCH --time=00:40:00

echo "Account: $SLURM_JOB_ACCOUNT"

if [[ -z "${SLURM_JOB_ID}" ]]; then
    export SLURM_ARRAY_TASK_ID=1
fi

echo "Activating conda environment"

# Activate conda environment
export CONDA_ROOT=$HOME/miniconda3
. $CONDA_ROOT/etc/profile.d/conda.sh
export PATH="$CONDA_ROOT/bin:$PATH"
conda activate thesis

echo "Starting python script"

python ./train_classifier.py "$SLURM_ARRAY_TASK_ID" "$APPROACH" "$DATA_DIR" "$MODELS_DIR"