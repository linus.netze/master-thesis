import json
import pandas as pd

from pathlib import Path
from typing import Callable
from re import Pattern

NEW_BRANDS = ["Nokia", "Sony", "HTC"]


def load_raw_review_data(data_path: Path) -> pd.DataFrame:
    # Load raw review dataset
    data = pd.read_csv(
        data_path,
        index_col=0,
    ).assign(phone_type=lambda df: df.phone_type.fillna(""))

    return data


def sanitize_brands(
    data: pd.DataFrame,
    mapping_dir: Path,
):
    manual_brand_map_path = mapping_dir / "brand_replacement_other.json"
    with open(manual_brand_map_path, "r") as f:
        manual_brand_map = json.load(f)

    fixed_brand_map_path = mapping_dir / "wrong_brand_assignments.json"
    with open(fixed_brand_map_path, "r") as f:
        fixed_brand_map = json.load(f)

    data = (
        data.pipe(replace_other, NEW_BRANDS, manual_brand_map)
        .pipe(fix_wrong_brand_assignments, fixed_brand_map)
        .pipe(fix_honor_brand)
        .pipe(fix_nexus_brand)
    )

    print(f"Number of instance pre-cleaning: {len(data)}")

    data = prune_by_brand_count(data)

    return data


def replace_other(
    df: pd.DataFrame,
    brands_automatic: list[str],
    manual_brand_mapping: dict[str, str],
) -> pd.DataFrame:
    """
    Replaces the 'Other' brand in the DataFrame with the corresponding brand
    based on automatically extracted brands for the brands given in `brands_automatic`
    and the mapping `manual_brand_mapping`.

    Args:
        df (pd.DataFrame): The input DataFrame.
        brands_automatic (list[str]): List of brands for automatic assignment.
        manual_brand_mapping (dict[str, str]): Mapping of phone types to brands.

    Returns:
        pd.DataFrame: The DataFrame with 'Other' brand replaced.

    """
    df = df.copy()

    df = _automatic_brand_assignment(df, brands_automatic)

    idx_brand_other = df["brand"] == "Other"
    phone_types = df.loc[idx_brand_other, "phone_type"]
    df.loc[idx_brand_other, "brand"] = phone_types.map(manual_brand_mapping)

    return df


def _automatic_brand_assignment(df: pd.DataFrame, brands: list[str]) -> pd.DataFrame:
    df = df.copy()

    other_idx = df.query("brand=='Other'").index

    def extract_brand(phone_type):
        for brand in brands:
            if brand.lower() in phone_type.lower():
                return brand

        return "Other"

    df.loc[other_idx, "brand"] = df.loc[other_idx, "phone_type"].apply(extract_brand)

    return df


def fix_wrong_brand_assignments(
    df: pd.DataFrame, fixed_brand_mapping: dict[str, dict[str, str]]
) -> pd.DataFrame:
    df = df.copy()

    for wrong_brand, mapping in fixed_brand_mapping.items():
        idx_wrong_brand = (df.brand == wrong_brand) & (
            df.phone_type.isin(mapping.keys())
        )
        df.loc[idx_wrong_brand, "brand"] = df.loc[idx_wrong_brand, "phone_type"].map(
            mapping
        )

    return df


def prune_by_brand_count(df: pd.DataFrame, min_count: int = 10) -> pd.DataFrame:
    """
    Prunes the DataFrame by removing all rows with a brand occurring less than `min_count` times.

    Parameters:
        df (pd.DataFrame): The input DataFrame.
        min_count (int): The minimum count of brand occurrences to keep.

    Returns:
        pd.DataFrame: The pruned DataFrame.
    """
    df = df.copy()
    brand_counts = df.brand.value_counts()
    idx_brand_too_small = df.brand.isin(brand_counts[brand_counts < min_count].index)
    df = df.loc[~idx_brand_too_small]

    return df


def fix_honor_brand(df: pd.DataFrame) -> pd.DataFrame:
    """
    Honor used to belong to Huawei and thus the brand name is often wrongly given.
    Fix the brand name for Honor phones in the given DataFrame.

    Parameters:
        df (pd.DataFrame): The input DataFrame containing phone data.

    Returns:
        pd.DataFrame: The DataFrame with the brand name fixed for Honor phones.
    """
    df = df.copy()
    # move honor phones from huawei to own brand
    idx_huawei_honor_phones = (df.brand == "Huawei") & (
        df.phone_type.str.contains("honor", case=False)
    )
    df.loc[idx_huawei_honor_phones, "brand"] = "Honor"

    return df


def fix_nexus_brand(df: pd.DataFrame) -> pd.DataFrame:
    """
    Moves Nexus phones from the 'Google' brand to their actual brand.

    Args:
        df (pd.DataFrame): The input DataFrame containing phone data.

    Returns:
        pd.DataFrame: The DataFrame with Nexus phones moved to their actual brand.

    """
    df = df.copy()

    nexus_brand_mapping = {
        "nexus 3": "Samsung",
        "nexus 5x": "LG",
        "nexus 5": "LG",
        "nexus 6": "Motorola",
    }

    idx_nexus_phones = df.phone_type.str.lower().isin(nexus_brand_mapping.keys()) & (
        df.brand == "Google"
    )

    df.loc[idx_nexus_phones, "brand"] = (
        df.loc[idx_nexus_phones, "phone_type"].str.lower().map(nexus_brand_mapping)
    )

    return df
