# Preprocessing

This folder contains the notebooks used for preprocessing the human reviews:

- `scrape_gsmarena.ipynb`: This notebook is used to scrape lists of phone model names for a given set of brands. Those names are required for normalizing the model names.
- `sanitize_phone_models.ipynb`: This notebook is used to standardize the model names in the human-written review dataset, i.e. "Samsung galaxy s5" and "Samsung s5" will both be standardized to "Samsung Galaxy S5".
- `preprocdessing.ipynb`: This notebook is used to conduct the remaining preprocessing on the human-written reviews.