# Review generation
This folder contains the scripts and notebooks used for generating reviews. 

## `setup_tasks.ipynb`
This notebook is used to split the process of generating the different review datasets into multiple tasks, which can be efficiently processed in parallel compute jobs on the RWTH cluster.

When using this notebook you have to set the global varibales `TASK_DATA_DIR` and `SOURCE`. `TASK_DATA_DIR` should be the path to the directory where the task definitions for WizardLM and GPT-4 as well as the generated reviews will be stored. `SOURCE` is the name for the LLM for which the tasks will be defined, for WizardLM set it to "hf" and for GPT-4 to "openai".

## `execute_task.py`
This script is used to generate the reviews for a single task as defined in the notebook `setup_tasks.ipynb`. It takes the following arguments:
- `task_id`: ID of the task to generate reviews for
- `model_type`: Either `huggingface` to generate the reviews with WizardLM or `openai` to generate them with GPT-4. When using GPT-4, an API token has to be provided in either the environment variables or a `.env` file (see [OpenAI Docs](https://github.com/openai/openai-python?tab=readme-ov-file#usage)).
- `--tasks_dir`: Path to the directory which contains the taks definitions and where the generated reviews will be stored.
- `--data_dir`: Path to the directory which contains the human reviews. They are required for the review generation to match the human review data.
- `--ignore_batches`: By setting this flag, the review generation will be conducted without batching, i.e. each batch only contains a single review.

## `job_openai.sh` and `job_huggingface.sh`
These scripts are used to submit the compute jobs for all review generation tasks to the RWTH Cluster. See [RWTH Cluster](https://help.itc.rwth-aachen.de/service/rhr4fjjutttf/article/6357a2a6944143a9867f71951e249737/) and [SLURM](https://slurm.schedmd.com/sbatch.html) docs for more information). Thereby each job calls `execute_task.py`for the respective task.

E.g. to submit the jobs for review generation with GPT-4, use the following commands:
```
export TASKS_DIR=${path to taskdir}
export DATA_DIR=${path to data dir}
sbatch -A ${RWTH Cluster account name} -o ${path for logfile} -a ${task ids for which to generate review} --export=TASKS_DIR,DATA_DIR job_openai.sh
```
`${path to data dir}` corresponds to the respective parameters of `execute_task.py`. `${path to taskdir}` should be set to the value used for `TASK_DATA_DIR` in `setup_tasks.ipynb`. See the SLURM docs for information on how to format the [logfile path](https://slurm.schedmd.com/sbatch.html#OPT_output) and [task ids](https://slurm.schedmd.com/sbatch.html#OPT_array).

## `postprocess.ipynb`
This notebook is used to postprocess the 
generated reviews, i.e. repairing the JSON if it is malformed.

Set `TASK_DATA_DIR` to the same value as used in `execute_task.py`.

## `eda.ipynb`
This notebook contains some exploratory data analysis of the generated reviews.

