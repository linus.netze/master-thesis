from typing import Any
import datasets.utils.logging as ds_logging


class DisableDatasetsProgressBar:
    def __enter__(self):
        ds_logging.disable_progress_bar()

    def __exit__(self, exc_type, exc_val, exc_tb):
        ds_logging.enable_progress_bar()
