from typing import Literal, NamedTuple
from pathlib import Path
import json

import pandas as pd

from setfit import SetFitModel
from transformers import PreTrainedModel, AutoModelForSequenceClassification

ModelType = SetFitModel | PreTrainedModel


class ClassificationCondition(NamedTuple):
    veracity: Literal["truthful", "deceptive"]
    strategy: str
    source: Literal["hf", "openai"]
    split_id: int
    split: dict[str, list[int]]


def get_condition(task_id: int, data_dir: Path) -> ClassificationCondition:
    conditions_path = data_dir / "classification" / "tasks.csv"

    conditions = pd.read_csv(conditions_path, index_col="task_id")
    condition_values = conditions.loc[task_id, :].to_dict()

    split_id = condition_values["split_id"]
    with open(data_dir / "classification" / "splits.json") as f:
        splits = json.load(f)
    split = splits[split_id]

    condition = ClassificationCondition(split=split, **condition_values)

    return condition

def load_model(model_dir: Path, model_type: str) -> tuple[ModelType, float]:

    if model_type == "setfit":
        model_class = SetFitModel
    elif model_type == "bert":
        model_class = AutoModelForSequenceClassification
    else:
        raise ValueError(f"Invalid model type '{model_type}'.")

    model = model_class.from_pretrained(model_dir)
    assert isinstance(model, ModelType)
    
    with open(model_dir / "inference_config.json") as f:
        config = json.load(f)
    
    threshold = config["classification_threshold"]

    return model, threshold
    