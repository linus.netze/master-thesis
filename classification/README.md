# Review generation
This folder contains the scripts and notebooks used for training and evaluating the classifiers.

## `setup.ipynb`
This notebook is used to create the 5-fold cross-validation splits and assign them to classification tasks, which can be efficiently processed in parallel compute jobs on the RWTH cluster.

## `train_classifier.py` & `job_train_classifiers.sh`
The script `train_classifier.py` is used to train a classifier on a single task as defined in the notebook `setup.ipynb`. It takes the following arguments:
- `task_id`: ID of the task on which the classifier is trained
- `approach`: Either `bert` to use BERT-style fine-tuning or `SetFit` for fine-tuning the classifier with SetFit.
- `--data_dir`: Path to the directory where all data is stored, i.e. which contains the prolific review as well as the LLM generated review datasets.
- `--models_dir`: Path to the directory in which the trained models should be stored.

In this thesis, `job_train_classifiers.sh` is used to submit compute jobs to RWTH cluster for executing `train_classifier.py` for each task.

To submit the jobs use the following commands:
```
export APPROACH=${finetuning approach}
export DATA_DIR=${path to data dir}
export MODELS_DIR=${path to models dir}
sbatch -A ${RWTH Cluster account name} -o ${path for logfile} -a ${task ids for which to train classifier} --export=APPROACH,DATA_DIR,MODELS_DIR job_train_classifiers.sh
```
Where `${finetuning approach}`, `${path to data dir}` and `${path to models dir}` correspond to the respective parameters of `train_classifier.py`. See the SLURM docs for information on how to format the [logfile path](https://slurm.schedmd.com/sbatch.html#OPT_output) and [task ids](https://slurm.schedmd.com/sbatch.html#OPT_array).


## `compare_classifiers.ipynb`
In this notebook the two fine-tuning approaches BERT-style fine-tuning and SetFit are compared.


## `amazon_inference.py` and `job_inference_amazon.sh`
The script `amazon_inference.py` is used to evaluate a single classifier on Amazon reviews. It takes the following arguments:
- `task_id`: ID of the task for which the classifier trained on this task should be evlauated on Amazon data.
- `approach`: Either `bert` to use BERT-style fine-tuning or `SetFit` for fine-tuning the classifier with SetFit.
- `data_dir`: Path to the directory where all data is stored, i.e. which contains the Amazon review as well as the LLM-generated review datasets and where the results of this evaluation will be stored.
- `models_dir`: Path to the directory in which the trained classifiers are stored.

In this thesis, `job_inference_amazon.sh` is used to submit compute jobs to RWTH cluster for executing `job_inference_amazon.py` for each task.

To submit the jobs use the following commands:
```
export DATA_DIR=${path to data dir}
export MODELS_DIR=${path to models dir}
sbatch -A ${RWTH Cluster account name} -o ${path for logfile} -a ${task ids for which to train classifier} --export=DATA_DIR,MODELS_DIR job_inference_amazon.sh
```
Where `${finetuning approach}`, `${path to data dir}` and `${path to models dir}` correspond to the respective parameters of `amazon_inference.py`. See the SLURM docs for information on how to format the [logfile path](https://slurm.schedmd.com/sbatch.html#OPT_output) and [task ids](https://slurm.schedmd.com/sbatch.html#OPT_array).


## `evaluation.ipynb`
In this notebook, the trained classifiers are evaluated with respect to the different research questions of this thesis.

## `calculate_shap.py` & `job_shap.sh`
The script `calculate_shap.py` is used to calculate SHAP values for a single classifier. It takes the following arguments:
- `task_id`: ID of the task for which the classifier trained on this task should be evlauated on Amazon data.
- `approach`: Either `bert` to use BERT-style fine-tuning or `SetFit` for fine-tuning the classifier with SetFit.
- `data_dir`: Path to the directory where all data is stored, i.e. which contains the Prolific review as well as the LLM-generated review datasets and where the results of this evaluation will be stored.
- `models_dir`: Path to the directory in which the trained models should be stored.

In this thesis, `job_shap.sh` is used to submit compute jobs to RWTH cluster for executing `calculate_shap.py` for each task.

To submit the jobs use the following commands:
```
export APPROACH=${finetuning approach}
export DATA_DIR=${path to data dir}
export MODELS_DIR=${path to models dir}
sbatch -A ${RWTH Cluster account name} -o ${path for logfile} -a ${task ids for which to train classifier} --export=APPROACH,DATA_DIR,MODELS_DIR job_openai.sh
```
Where `${finetuning approach}`, `${path to data dir}` and `${path to models dir}` correspond to the respective parameters of `calculate_shap.py`. See the SLURM docs for information on how to format the [logfile path](https://slurm.schedmd.com/sbatch.html#OPT_output) and [task ids](https://slurm.schedmd.com/sbatch.html#OPT_array).

## `interpretability.ipynb`
In this notebook the SHAP values are evaluated.

## `train_plots.ipynb`
This notebook contains the code for creating a figure displaying the loss curves during classifier training.

