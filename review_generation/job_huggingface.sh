#!/usr/bin/zsh

#SBATCH -J review_generation
#SBATCH --gres=gpu:2
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=100G
#SBATCH --time=01:00:00

echo "Starting task $SLURM_ARRAY_TASK_ID"
echo "Account: $SLURM_JOB_ACCOUNT"

echo "Activating conda environment"

# Activate conda environment
export CONDA_ROOT=$HOME/miniconda3
. $CONDA_ROOT/etc/profile.d/conda.sh
export PATH="$CONDA_ROOT/bin:$PATH"
conda activate thesis

echo "Starting python script"

python execute_task.py "$SLURM_ARRAY_TASK_ID" "huggingface" --tasks_dir "$TASKS_DIR/hf_tasks" --data_dir "$DATA_DIR"