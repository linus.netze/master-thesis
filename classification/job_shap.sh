#!/usr/bin/zsh

#SBATCH -J shap
#SBATCH --account=thes1622
#SBATCH -o /home/ln908953/Documents/master_thesis/repo/data/classification/logs/shap/%a_%A.log
#SBATCH --gres=gpu:1
#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=48G
#SBATCH --time=02:00:00
#SBATCH --array=0-4,10-14,40-44,50-54

echo "Account: $SLURM_JOB_ACCOUNT"

if [[ -z "${SLURM_JOB_ID}" ]]; then
    export SLURM_ARRAY_TASK_ID=1
fi

echo "Activating conda environment"

# Activate conda environment
export CONDA_ROOT=$HOME/miniconda3
. $CONDA_ROOT/etc/profile.d/conda.sh
export PATH="$CONDA_ROOT/bin:$PATH"
conda activate thesis

echo "Starting python script"

python ./calculate_shap.py "$SLURM_ARRAY_TASK_ID" "bert" "/home/ln908953/Documents/master_thesis/repo/data" "/hpcwork/thes1622/models"