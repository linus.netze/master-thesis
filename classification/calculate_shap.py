import argparse
import pickle
from pathlib import Path
from typing import NamedTuple

import pandas as pd
import shap

import datasets
from datasets import Dataset
from transformers import pipeline

from thesis.classification import (
    get_condition,
    ClassificationCondition,
    get_model_dir,
    get_evaluation_dir,
    ModelType,
    load_model,
    CLASS_LABELS,
    load_data,
    get_condition_dataset,
)
from thesis.classification.utils import DisableDatasetsProgressBar


class Inputs(NamedTuple):
    approach: str
    cond: ClassificationCondition
    data_dir: Path
    model_dir: Path
    evaluation_dir: Path


def _get_inputs() -> Inputs:
    parser = argparse.ArgumentParser()
    parser.add_argument("task_id", type=int)
    parser.add_argument("approach")
    parser.add_argument("data_dir", type=Path)
    parser.add_argument("models_dir", type=Path)

    args = parser.parse_args()
    cond = get_condition(args.task_id, args.data_dir)

    inputs = Inputs(
        approach=args.approach,
        cond=cond,
        model_dir=get_model_dir(args.models_dir, args.approach, cond),
        data_dir=args.data_dir,
        evaluation_dir=get_evaluation_dir(args.data_dir, args.approach, cond),
    )

    if not inputs.evaluation_dir.exists():
        inputs.evaluation_dir.mkdir(parents=True)

    return inputs


def _load_amazon_data(data_dir: Path) -> Dataset:
    reviews = pd.read_csv(data_dir / "human" / "review_data_preprocessed.csv")
    amazon_reviews = (
        reviews.loc[lambda df: ~df.matched_review_id.isna()]
        .rename(columns={"review_title": "title", "review_text": "text"})
        .assign(
            review_id=lambda df: df.matched_review_id.astype(int),
            label="human",
            source="amazon",
        )
        .loc[:, ["review_id", "text", "label", "source"]]
    )

    # Transform to HuggingFace Dataset
    with DisableDatasetsProgressBar():
        amazon_data = Dataset.from_pandas(
            amazon_reviews, preserve_index=False
        ).cast_column("label", CLASS_LABELS)
    return amazon_data


def _load_dataset(
    data_dir: Path, cond: ClassificationCondition
) -> tuple[Dataset, pd.DataFrame]:
    data = load_data(data_dir)
    test_data = get_condition_dataset(data, cond)["test"]
    amazon_data = _load_amazon_data(data_dir)

    dataset = datasets.concatenate_datasets([test_data, amazon_data])

    mapping = dataset.select_columns(
        ["review_id", "label", "strategy", "source", "veracity_label"]
    ).to_pandas()

    return dataset, mapping


def _setup_shap_explainer(model: ModelType) -> shap.Explainer:
    pipe = pipeline(
        "text-classification",
        model=model,
        tokenizer="roberta-large",
        device="cuda",
        truncation=True,
        top_k=None,
    )

    modelp = shap.models.TransformersPipeline(pipe, rescale_to_logits=True)
    explainer = shap.Explainer(modelp, pipe.tokenizer, seed=42)

    return explainer


def _save_shap_values(
    shap_values: shap.Explanation, id_mapping: pd.DataFrame, evaluation_dir: Path
):
    path = evaluation_dir / "shap_values.pkl"
    with open(path, "wb") as f:
        pickle.dump(shap_values, f)

    id_mapping.to_csv(evaluation_dir / "shap_value_id_mapping.csv")


def main():
    inputs = _get_inputs()

    dataset, id_mapping = _load_dataset(inputs.data_dir, inputs.cond)
    model, _ = load_model(inputs.model_dir, inputs.approach)
    explainer = _setup_shap_explainer(model)
    shap_values = explainer(dataset["text"])

    _save_shap_values(shap_values, id_mapping, inputs.evaluation_dir)


if __name__ == "__main__":
    main()
